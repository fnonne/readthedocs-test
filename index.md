# AIM



**ADABUS Srl** - via Santi Nabore e Felice 7 - 20147 Milano (Italy) - tel: +39 0248027177 - web: [www.adabus.it](http://www.adabus.it) - email [info@adabus.it](mailto:info@adabus.it) ![logo](pics/LogoAdabus.gif)



Document info:

| PROJECT NAME | AIM                      |
| ------------ | ------------------------ |
| CUSTOMER     | Adabus                   |
| END CUSTOMER |                          |
| DOCUMENT     | Technical Specification  |
| VERSION      | 2.0.0-0-std              |
| DATE         | 2017-11-08               |
| FILE         | AIMv2_techspec_v2.0.0.md |
| AUTHOR       | Franco Nonne             |



Revision history:

| DATE       | VERSION     | AUTHOR       | NOTE                               |
| :--------- | ----------- | ------------ | ---------------------------------- |
| 2017-11-03 | 2.0.0.0-std | Franco Nonne | Full documentation – first release |



Related software:

| DATE       | VERSION | SOFTWARE         |
| :--------- | ------- | ---------------- |
| 2017-11-03 | 2.0.0   | AIM Server       |
| 2017-11-03 | 2.0.0   | AIM Watchdog     |
| 2017-11-03 | 2.0.0   | AIM Shell Client |
| 2017-11-03 | 2.0.0   | AIM Test         |





# Contents


[TOC]







# Introduzione

AIM (Adabus Integration Manager) è la soluzione per le esigenze di integrazione di applicativi esterni con i recorder della famiglia MARATHON di ASC Telecom AG, incluso EVOip Server per la registrazione  di comunicazioni voce su IP.

Il software può essere utilizzato in diversi ambiti e con diverse modalità di integrazione, al fine di controllare la registrazione e di effettuare la marcatura delle chiamate con dati applicativi. Il software sostituisce e migliora le funzionalità rese disponibili dall'interfaccia ESS API di ASC.

Il presente documento descrive l'architettura del software AIM Server ed il protocollo per la comunicazione client/server, incluse le linee guida per l'integrazione dei Client.





# Architettura e specifiche funzionali

## Architettura generale

AIM (*Adabus Integration Manager*) è un'applicazione di servizio installata come modulo aggiuntivo su una macchina ASC MARATHON EVOLUTION, EVOlite o EVOip Server. 

Il software è diviso nelle seguenti componenti:

*   AIM Server: il componente principale, cui si connettono i Client per usufruire dei servizi di integrazione
*   AIM Watchdog: componente di controllo che verifica la raggiungibilità del servizio, in grado  di effettuare azioni configurabili in caso di ripetuti fault.

AIM Server comunica con l'ambiente informatico del cliente su rete LAN Ethernet. AIM Server gestisce la comunicazione client/server con i Client (e.g software di barra telefonica, sistema IVR, etc...), con un protocollo basato su tcp/ip. Si rimanda alle sezioni seguenti per approfondimenti sulle funzionalità rese disponibili da AIM Server, sul protocollo e sulle modalità di comunicazione tra Client e Server.

Il diagramma seguente mostra l'architettura generale del software AIM:


<img src="pics/AIM_Architecture.png" width="800px" />


*Figura 1: Architettura generale AIM Server*

Descrizione dei componenti:

*   Telephony system: il sistema telefonico del cliente, dotato di interfacciamento CTI per l'integrazione con l'ambiente informatico, i cui telefoni sono registrati dal sistema ASC MARATHON.

*   CTI Client: il software di integrazione che si occupa di ricevere e gestire il flusso telefonico di uno o più terminali telefonici, e di comunicare con il Server AIM per con il controllo e la marcatura delle registrazioni delle chiamate in corso.

*   ASC MARATHON Recorder: il sistema di registrazione delle comunicazioni telefoniche. Il sistema è altamente modulare e flessibile; tra i moduli interni, in particolare, l'ASC API Server si occupa di gestire l'integrazione con software esterni via interfacce API.

*   AIM Server: il modulo per l'integrazione con i software esterni, oggetto del presente documento. AIM Server è connesso all'ASC API Server tramite un'interfaccia ASC denominata ASC S&R API Java, mediante la quale riceve gli eventi di registrazione (da non confondere con gli eventi CTI che riceve il CTI Client). AIM Server riceve dal CTI Client, tramite un protocollo ad hoc (AIM protocol) descritto nei paragrafi seguenti, i comandi per il controllo delle registrazioni e la marcatura.

*   AIM Watchdog: si occupa di verificare lo stato di servizio del Server AIM ad intervalli regolari. In caso di fault ripetuti si occupa di avviare uno script di servizio completamente configurabile (ad esempio è possibile riavviare AIM, oppure inviare una notifica, etc...).





## Architettura di monitoring

È  possibile integrare AIM con un sistema di monitoraggio preesistente in grado di ricevere SNMP trap. Il diagramma seguente mostra l'architettura del sistema di monitoraggio:




<img src="pics/AIM_monitoring.png" width="800px" />



*Figura 2: Architettura del sistema di monitoraggio*



Descrizione dei componenti:

*   SNMP Node Manager: il sistema di monitoraggio che riceve le trap dai vari sistemi in rete.

*   ASC MARATHON Recorder: il sistema di registrazione delle comunicazioni telefoniche. 

*   AIM Server: il servizio può generare trap relative al funzionamento interno dei vari moduli. Le trap, altamente configurabili, vengono inviate al SNMP Node Manager.

*   AIM Watchdog: si occupa di verificare lo stato di servizio del Server AIM ad intervalli regolari. Oltre al normale funzionamento di verifica del servizio AIM Server, può generare trap ed inviarle al SNMP Node Manager.

*   AIM Monitor: script opzionale per controllare che i due servizi AIM Server e AIM Watchdog siano attivi. In caso negativo è in grado di riavviare AIM Watchdog e di generare ulteriori trap da inviare al SNMP Node Manager.

    ​

## Panoramica delle funzionalità

AIM Server svolge le funzioni di seguito elencate:

*   Gestire la comunicazione client/server con il software di barra telefonica: gestione di connessioni multiple TCP con scambio di messaggi contenenti comandi di *RecordOnDemand* e dati di marcatura per le registrazioni (AIM Protocol). La gestione della comunicazione include:
    *   l'utilizzo di un thread dedicato per ogni connessione;
    *   la gestione automatica delle risorse relative alle connessioni dedicate.
*   Gestire la comunicazione server/server con software CTI: gestione di connessioni multiple TCP con scambio di messaggi contenenti comandi di *RecordOnDemand* e dati di marcatura per le registrazioni (AIM Protocol). In modalità server/server a differenza della modalità di default client/server, le richieste e le risposte del protocollo sono gestite in modo asincrono mediante l'utilizzo di un worker thread dedicato.
*   Eseguire i comandi di controllo registrazione relativi alle chiamate in corso. A seconda della tipologia di integrazione e alle esigenze di progetto specifiche, è possibile richiedere l'esecuzione dei seguenti comandi per il controllo della registrazione:
    *   Start/Stop (controllo avvio registrazione);
    *   Mute/UnMute (controllo interruzione audio);
    *   Keep/Delete (controllo salvataggio completo registrazione).
*   Eseguire i comandi di marcatura (*tag*)* *relativi a chiamate in corso con i dati forniti dal Client. È possibile utilizzare i seguenti dati di marcatura:
    *   AgentID
    *   Own Phone Number
    *   Partner Phone Number
    *   Call Direction
    *   Comment
    *   External CallID
    *   Text1 … Text20
    *   Long1 … Long10
*   Eseguire comandi di controllo/verifica dello stato di singoli canali o del servizio AIM:
    *   stato di uno specifico canale (*GetChannelState*);
    *   stato del Server AIM (*isalive*);
    *   verifica che una extension o un indirizzo ip siano configurate per la registrazione (*CheckExtension, CheckIp*).
*   Riavviare automaticamente, ad intervalli regolari, la registrazione su alcuni specifici canali configurati (*AutoRestart*).
*   Leggere ad intervalli configurabili la configurazione statica dei canali (eventuali DN ed ip associati staticamente).
*   Verificare lo stato di connessione con il modulo ASC API Server del recorder e riconnettersi in automatico in caso di errore.
*   Creare file di log dettagliati per consentire la verifica dell'attività del servizio.
*   Inviare trap SNMP per l'integrazione in sistemi di monitoraggio.




AIM Watchdog svolge le seguenti funzioni: 

*   Verifica periodica dello stato del Server AIM, tramite invio di richieste *isalive*.
*   Riavvio automatico del Server AIM in caso di fault ripetuti.
*   Creare file di log dettagliati per consentire la verifica dell'attività del servizio.
*   Inviare trap SNMP per l'integrazione in sistemi di monitoraggio.



Oltre ad AIM Server e ad AIM Watchdog, sono disponibili anche i seguenti strumenti:

*   AIM ShellClient: semplice client per inviare comandi ad AIM Server dalla shell.
*   AIM Test: applicazione per verificare le funzionalità sel sistema Possono essere testati tutti i comandi di AIM Protocol.
*   AIM Monitor: script che permette un ulteriore livello di monitoring sui due servizi AIM Server e AIM Watchdog.

    ​

## Specifiche di comunicazione (modalità client/server)

La comunicazione tra i Client e AIM Server avviene con le seguenti modalità:

1.  AIM Server apre una porta in modalità *listen* in attesa di una connessione da parte dei Client. 
2.  Il Client effettua una connessione TCP sulla porta in attesa. Ogni connessione attiva è gestita dal Server con un *thread* dedicato.
3.  Il Client invia un messaggio di richiesta ad AIM Server. 
4.  AIM Server risponde con un messaggio (*acknowledgement*) che reca diverse informazioni sulla richiesta inviata, tra cui un codice di ritorno univoco indicante il risultato della richiesta effettuata.
5.  Il Client riceve la risposta.
6.  Il Client connesso ha la possibilità eventualmente di effettuare altre richieste (ciclo punti 3-4).
7.  Al termine il Client si disconnette, ed il Server libera le risorse ad esso dedicate.
8.  Se il Client non effettua la disconnessione e non invia al Server nuovi comandi in un determinato periodo temporale (configurabile), AIM Server chiude il socket di comunicazione e libera il thread e le risorse dedicate. 

I dettagli del protocollo di comunicazione sono descritti al paragrafo [Protocollo di comunicazione](#protocollo-di-comunicazione)



## Specifiche di comunicazione (modalità server/server)


Opzionalmente AIM Server può lavorare in modalità server/server; in questo caso la comunicazione avviene con modalità diversa rispetto a quanto esposto nel paragrafo precedente:

1.  AIM Server apre una porta in modalità *listen* in attesa di una connessione da parte dei Server.
2.  Il Server effettua una connessione TCP sulla porta in attesa.
3.  AIM Server gestisce ogni nuova connessione con un thread dedicato; viene inoltre creato un pool di worker thread per gestire la coda delle richieste inviate dal Server.
4.  Il Server invia le richieste ad AIM Server. 
5.  Ogni richiesta inviata ad AIM Server viene messa in coda per l'esecuzione. AIM Server libera subito le risorse su socket e continua a leggere le richieste inviate.
6.  La coda viene gestita dai worker thread che a turno si occupano di eseguire i comandi e di inviare le risposte ai Server connessi sui relativi socket.
7.  Al termine il Server si disconnette, ed AIM Server libera le risorse ad esso dedicate.
8.  Se il Server non effettua la disconnessione e non invia ad AIM Server nuovi comandi in un determinato periodo temporale (configurabile), AIM Server chiude il socket di comunicazione e libera i thread e le risorse dedicate. 

I dettagli del protocollo di comunicazione  sono descritti al paragrafo [Protocollo di comunicazione](#protocollo-di-comunicazione).

Per attivare questa modalità di comunicazione è necessario impostare il parametro *ServerToServerMode* (si veda il paragrafo [Configurazione di AIM Server](#configurazione-di-AIM-Server)).







# Protocollo di comunicazione

## Elenco dei messaggi

Il protocollo di comunicazione AIM Protocol prevede le seguenti tipologie di messaggi: 

1.  Richiesta di Start ([*StartRequest*](#Messaggio *StartRequest*)): messaggio Client➞Server. Il Client richiede al Server di avviare la registrazione di una certa chiamata in corso, indicando come deve essere identificata la chiamata. 

2.  Risposta alla richiesta di Start ([*StartAnswer*](#Messaggio *StartAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

3.  Richiesta di ForceStart ([*ForceStartRequest*](#Messaggio *ForceStartRequest*)): messaggio Client➞Server. Il Client richiede al Server di avviare la registrazione su un certo canale. In questo caso la registrazione viene avviata anche in assenza di una conversazione in corso. 

4.  Risposta alla richiesta di ForceStart ([*ForceStartAnswer*](#Messaggio *ForceStartAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

5.  Richiesta di Stop ([*StopRequest*](#Messaggio *StopRequest*)): messaggio Client➞Server. Il Client richiede al Server di fermare la registrazione in corso, indicando come deve essere identificata la chiamata.

6.  Risposta alla richiesta di Stop ([*StopAnswer*](#Messaggio *StopAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

7.  Richiesta di Mute ([*MuteRequest*](#Messaggio *MuteRequest*)): messaggio Client➞Server. Il Client richiede al Server di inserire silenzio durante la registrazione in corso, indicando come deve essere identificata la chiamata.

8.  Risposta alla richiesta di Mute ([*MuteAnswer*](#Messaggio *MuteAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

9.  Richiesta di UnMute ([*UnMuteRequest*](#Messaggio *UnMuteRequest*)): messaggio Client➞Server. Il Client richiede al Server di interrompere l'inserimento del silenzio e proseguire con la normale registrazione, indicando come deve essere identificata la chiamata. 

10.  Risposta alla richiesta di UnMute ([*UnMuteAnswer*](#Messaggio *UnMuteAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

11.  Richiesta di Keep ([*KeepRequest*](#Messaggio *KeepRequest*)): messaggio Client➞Server. Il Client richiede al Server di salvare la registrazione di una certa chiamata in corso, indicando come deve essere identificata la chiamata. 

12.  Risposta alla richiesta di Keep ([*KeepAnswer*](#Messaggio *KeepAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

13.  Richiesta di Delete ([*DeleteRequest*](Messaggio *DeleteRequest*)): messaggio Client➞Server. Il Client richiede al Server di non salvare la registrazione di una certa chiamata in corso, indicando come deve essere identificata la chiamata. 

14.  Risposta alla richiesta di Delete ([*DeleteAnswer*](#Messaggio *DeleteAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

15.  Richiesta di Tag ([*TagRequest*](#Messaggio *TagRequest*)): messaggio Client➞Server. Il Client richiede al Server di marcare una certa registrazione in corso, indicando come deve essere identificata la chiamata e quali sono i dati di marcatura da associare alla registrazione.

16.  Risposta alla richiesta di Tag ([*TagAnswer*](#Messaggio *TagAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

17.  Richiesta di GetChannelState ([*GetChStateRequest*](#Messaggio *GetChStateRequest*)): messaggio Client➞Server. Il Client richiede al Server lo stato di uno specifico canale. 

18.  Risposta alla richiesta di GetChannelState ([*GetChStateAnswer*](#Messaggio *GetChStateAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

19.  Richiesta di CheckExtension ([*CheckExtRequest*](#Messaggio *CheckExtRequest*)): messaggio Client➞Server. Il Client richiede al Server se una specifica Extension è configurata per l'utilizzo con il recorder.

20.  Risposta alla richiesta di CheckExtension ([*CheckExtAnswer*](#Messaggio *CheckExtAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

21.  Richiesta di CheckIp ([*CheckIpRequest*](#Messaggio *CheckIpRequest*)): messaggio Client➞Server. Il Client richiede al Server se uno specifico indirizzo ip è configurato per la registrazione.

22.  Risposta alla richiesta di CheckIp ([*CheckIpAnswer*](#Messaggio *CheckIpAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

23.  Richiesta IsAlive ([*IsAliveRequest*](#Messaggio *IsAliveRequest*)): messaggio Client➞Server. Il Client richiede al Server lo stato di servizio corrente.  

24.  Risposta alla richiesta di IsAlive ([*IsAliveAnswer*](#Messaggio *IsAliveAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

25.  Richiesta Watchdog ([*WatchdogRequest*](#Messaggio *WatchdogRequest*)): messaggio Client➞Server. È la richiesta inviata periodicamente dal servizio Watchdog al Server, per verificare lo stato di servizio.

26.  Risposta alla richiesta Watchdog ([*WatchdogAnswer*](#Messaggio *WatchdogAnswer*)): messaggio Server➞Client. Il Server risponde con un messaggio contenente le informazioni sull'esito della richiesta.

     ​



## Formato dei messaggi

Le richieste e le relative risposte descritte al paragrafo precedente utilizzano messaggi a lunghezza variabile, con il seguente formato: 

```
MessageSize|XMLMessage
```

| Nome del parametro | Dimensione | Descrizione                              |
| ------------------ | ---------- | ---------------------------------------- |
| MessageSize        | 4 bytes    | Dimensione del messaggio seguente XMLMessage. É un intero di 4 byte espresso in network byte order. |
| XMLMessage         | Variabile  | Messaggio, formattato come documento XML standard, secondo le specifiche riportate nei paragrafi seguenti. |




**Nota**:

Il messaggio è composto da una parte binaria (*MessageSize*) e da una parte testuale (*XMLMessage*) contenente il messaggio vero e proprio. Si rimanda all'[Appendice A](#Appendice A: Metodi di esempio per la comunicazione client/server) per un'esempio di metodi (implementazione in Java) per la comunicazione corretta tra Client e Server.



## Formato del messaggio XML

La parte principale dei messaggi client/server è l'XMLMessagge cui si è accennato nel paragrafo precedente.  Il *Document Type Definition* è il seguente:

```
<!DOCTYPE message [ 
<!ELEMENT message (action, target?, tagging?, result?)> 
<!ATTLIST message type (request|answer) #REQUIRED>
<!ATTLIST message version CDATA "1.21"> 
<!ELEMENT action (#PCDATA)> 
<!ELEMENT target (#PCDATA)> 
<!ATTLIST target type (channelid|dn|external_phone|dtmf_sequence|configured_ip|configured_dn) #REQUIRED> 
<!ELEMENT tagging (agent?, own_phone?, partner_phone?, call_direction?, comment?, external_callid?, ascii, long)> 
<!ELEMENT agent (#PCDATA)> 
<!ELEMENT own_phone (#PCDATA)> 
<!ELEMENT partner_phone (#PCDATA)> 
<!ELEMENT call_direction (#PCDATA)> 
<!ELEMENT comment (#PCDATA)> 
<!ELEMENT external_callid (#PCDATA)> 
<!ELEMENT ascii (#PCDATA)> 
<!ATTLIST ascii index (1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20) #REQUIRED> 
<!ELEMENT long (#PCDATA)> 
<!ATTLIST long index (1|2|3|4|5|6|7|8|9|10) #REQUIRED> 
<!ELEMENT result (code, description, channel*)>
<!ELEMENT code (#PCDATA)> 
<!ELEMENT description (#PCDATA)> 
<!ELEMENT channel (chstate, storage_state?, callid?, external_callid?, starttime?, own_phone?, partner_phone?, thirdparty_phone?, call_direction?)>
<!ATTLIST channel id CDATA #REQUIRED> 
<!ELEMENT chstate (#PCDATA)>
<!ELEMENT storage_state (#PCDATA)>
<!ELEMENT callid (#PCDATA)> 
<!ELEMENT starttime (#PCDATA)> 
<!ELEMENT thirdparty_phone (#PCDATA)> 
]>
```



Di seguito è riportata la descrizione degli elementi:



#####`<message>`

Indica un messaggio. Contiene 4 diversi sottoelementi: 

*  `<action>`  (obbligatorio),
*  `<target>`  (opzionale), 
*  `<tagging>`  (opzionale), 
*  `<result>` (opzionale). 

L'elemento message comprende l'attributo `type` (obbligatorio), il quale può assumere uno dei seguenti valori:
*  `"request"`: il messaggio è una richiesta del Client.
*  `"answer"`: il messaggio è la risposta del Server ad una specifica richiesta. 

L'elemento inoltre complende l'attributo `version` (opzionale), che indica la versione di AIM protocol utilizzata dal client; se l'attributo non viene specificato nel messaggio del client, oppure se la versione del messaggio non è quella corrente, il server considera implicitamente il valore di default (`"2.0"`).



##### `<action>` 

Indica il tipo di azione cui si riferisce il messaggio. I valori possibili sono i seguenti:

* `start`: si riferisce al messaggio *StartRequest* (o *StartAnswer*).
* `forcestart`: si riferisce al messaggio *ForceStartRequest* (o *ForceStartAnswer*).
* `stop`: si riferisce al messaggio *StopRequest* (o *StopAnswer*).
* `keep`: si riferisce al messaggio KeepRequest (o *KeepAnswer*)
* `delete`: si riferisce al messaggio *DeleteRequest* (o *DeleteAnswer*)
* `mute`: si riferisce al messaggio *MuteRequest* (o *MuteAnswer*)
* `unmute`: si riferisce al messaggio *UnMuteRequest* (o *UnMuteAnswer*)
* `tag`: si riferisce al messaggio *TagRequest* (o *TagAnswer*). 
* `getchstate`: si riferisce al messaggio *GetChStateRequest* (o *GetChStateAnswer*).
* `checkext`: si riferisce al messaggio *CheckExtRequest* (o *CheckExtAnswer*)
* `checkip`: si riferisce al messaggio *CheckIpRequest* (o *CheckIpAnswer*)
* `isalive`: si riferisce al messaggio *IsAliveRequest* (o *IsAliveAnswer*).
* `watchdog`: si riferisce al messaggio *WatchdogRequest* (o *WatchdogAnswer*).



##### `<target>` 

Elemento utilizzato per identificare la chiamata. 

L'attributo `type` (obbligatorio) specifica ulteriormente la tipologia di dato, e può assumere uno dei seguenti valori:

* `"channelid"`: il tipo di dato passato è l'identificativo univoco del canale di registrazione.
* `"dn"`: il tipo di dato passato è l'extension (DN) univoca dell'operatore.
* `"external_phone"`:  il tipo di dato passato è il numero di telefono del chiamante/chiamato.
* `"dtmf_sequence"`: il tipo di dato passato è una sequenza di toni DTMF che identifica univocamente una chiamata in corso.
* `"configured_ip"`: il tipo di dato passato è l'indirizzo ip con cui è stato configurato univocamente il canale.
* `"configured_dn"`: il tipo di dato passato è l'extension con cui è stato configurato univocamente il canale.

Il valore di target dipende ovviamente dalla tipologia del dato espressa dall'attributo type. Per maggiori dettagli si rimanda al paragrafo [Identificazione della chiamata](#Identificazione della chiamata).



##### `<tagging>`

Elemento composto da più sottoelementi i quali costituiscono la marcatura dati da effettuare sulla chiamata oggetto dei comandi *Start*, *ForceStart*, *Stop*, *Keep* e *Tag*. 

I sottoelementi sono i seguenti: 

* `<agent>` (opzionale),  
* `<own_phone>` (opzionale), 
* `<partner_phone>` (opzionale), 
* `<call_direction>` (opzionale),  
* `<comment>` (opzionale), 
* `<external_callid>` (opzionale), 
* `<ascii>` (opzionale), 
* `<long>` (opzionale).




##### `<agent>`

Elemento che identifica un Agente. Il valore verrà salvato sul database del MARATHON nel campo *AgentID*.



##### `<own_phone>`
Elemento che identifica il numero di telefono "interno" (il numero chiamato oppure l'extension dell'operatore). Il valore verrà salvato sul database del MARATHON nel campo *OwnPhoneNumber*.



##### `<partner_phone>`
Elemento che identifica il numero di telefono "esterno" (numero del chiamante/chiamato). Il valore verrà salvato sul database del MARATHON nel campo *PartnerPhoneNumber*.



##### `<call_direction>` 
Elemento che identifica la direzione della chiamata. Può assumere uno dei seguenti valori:
* `"inbound"`: la chiamata è esterna in ingresso, 
* `"outbound"`: la chiamata è esterna in uscita, 
* `"incoming"`: la chiamata è interna in ingresso,
* `"outgoing"`: la chiamata è interna in uscita.




##### `<comment>`

Elemento che identifica un dato applicativo associato alla chiamata (liberamente utilizzabile). Il valore verrà salvato sul database del MARATHON nel campo *CallComment*.



##### `<external_callid>` 
Elemento che identifica il CallID univoco della chiamata. Si tratta dell'identificativo di pertinenza dell'applicativo esterno, non è da confondere con l'identificativo generato dal recorder. 
Il valore verrà salvato sul database del MARATHON in uno dei campi *Text1 ... Text20* a disposizione (il campo di destinazione è configurabile).



##### `<ascii>`
Elemento che identifica un dato applicativo associato alla chiamata (liberamente utilizzabile). Il valore verrà salvato sul database del MARATHON nei campi *Text1 … Text20*. 
L'attributo `index` (obbligatorio) indica il numero del campo relativo (ad esempio: `<ascii index=”1”>` indica il campo *Text1*). Possono essere specificati diversi elementi `<ascii>`.




##### `<long>`
Elemento che identifica un dato applicativo associato alla chiamata (liberamente utilizzabile). Sono utilizzabili solamente valori numerici. Il valore verrà salvato sul database del MARATHON nei campi *Number1 ... Number10*.
L'attributo `index` (obbligatorio) indica il numero del campo relativo (ad esempio: `<long index=”1”>` indica il campo *Number1*). Possono essere specificati diversi elementi `<long>`.



##### `<result>`

Elemento composto da più sottoelementi, che si riferisce all'esito della richiesta effettuata. Viene valorizzato solamente dai messaggi il cui type è `“answer”`.
I sottoelementi sono i seguenti:
* `<code>`
* `<description>`
* `<channel>` (opzionale; in alcuni casi può essere multiplo).




##### `<code>`

Codice di ritorno, indica il risultato dell'azione richiesta.



##### `<description>`

Descrizione del risultato ottenuto.



##### `<channel>`
Elemento composto da più sottoelementi che descrivono lo stato di uno specifico canale.
L'elemento comprende l'attributo `id` (obbligatorio), valorizzato con l'identificativo univoco del canale di registrazione (origine: recorder MARATHON).
L'elenco dei sottoelementi è il seguente:
* `<chstate>`,
* `<storage_state>` (opzionale),
* `<callid>`,
* `<external_callid>` (opzionale),
* `<starttime>` (opzionale),
* `<own_phone>` (opzionale),
* `<partner_phone>` (opzionale),
* `<thirdparty_phone>` (opzionale),
* `<call_direction>` (opzionale),

A livello di configurazione è possibile scegliere se AIM Server debba restituire o meno alcuni dettagli della chiamata. Nel caso in cui tali dettagli non fossero valorizzati, il server non riporta il relativo tag. Per maggiori dettagli si rimanda al paragrafo [Configurazione di AIM Server](#Configurazione di AIM Server).



##### `<chstate>`

Elemento che indica lo stato del canale.  Può assumere i seguenti valori:
* `idle`: nessuna chiamata in corso;
* `active`: la chiamata è attiva, ma il canale non è stato avviato;
* `recording`: la chiamata è in registrazione;
* `muted`: la chiamata è in pausa registrazione.




##### `<storage_state>`

Elemento che indica la tipologia di registrazione attiva sul canale.
Può assumere i seguenti valori:

* `direct`: la registrazione è direttamente salvata (*SaveOnDemand* non attivo);
* `keep`: in modalità *SaveOnDemand*, indica che la registrazione verrà salvata alla chiusura;
* `delete`: in modalità *SaveOnDemand*, indica che la registrazione verrà eliminata alla chiusura.




##### `<callid>`

Contiene il valore dell'identificativo univoco della chiamata (origine: recorder).



##### `<external_callid>`

Contiene il valore dell'identificativo univoco della chiamata (origine: esterna la recorder) precedentemente marcato.



##### `<starttime>`

Contiene il timestamp dell'avvio della registrazione sullo specifico canale. Il server fornisce il dato solo se la funzione è attiva (configurabile).



##### `<own_phone>`

Contiene il valore del campo *OwnPhoneNumber* (numero di telefono proprio, DN) associato alla chiamata.



##### `<partner_phone>`

Contiene il valore del campo *PartnerPhoneNumber* (numero di telefono esterno) associato alla chiamata.



##### `<thirdparty_phone>`

Contiene il valore del campo *ThirdPartyPhoneNumber* (altri numeri) associato alla chiamata.



##### `<call_direction>`

Contiene la direzione della chiamata.



**Nota**:

L'intestazione standard dei messaggi xml, contenente la dichiarazione del tipo di protocollo, la versione ed il relativo encoding, viene omessa per semplicità in tutti i messaggi di richiesta e di risposta. Per tutti  i messaggi viene considerata di default la seguente:

`<?xml version="1.0" encoding="iso-8859-1"?> `

Per quanto riguarda la codifica dei caratteri, il default utilizzato è ISO-8859-1 (Latin-1) con caratteri a byte singolo.

Il messaggio ricevuto dal client viene elaborato con un parser XML; in caso di errore di validazione dello stesso, AIM Server non può inviare una risposta (in quanto la risposta è sempre legata allo schema della richiesta), pertanto chiude la connessione con il client.

Si raccomanda di utilizzare le normali regole per la costruzione di messaggi XML ben formati

*   utilizzare sempre il tag di chiusura
*   i tag devono essere correttamente nidificati
*   gli attributi devono essere racchiusi tra doppi apici
*   gli elementi non possono contenere i seguenti caratteri, che vanno sostituiti con la relativa sequenza di escape:

| simbolo | escape   |
| ------- | -------- |
| `<`     | `&lt;`   |
| `>`     | `&gt;`   |
| `&`     | `&amp;`  |
| `'`     | `&apos;` |
| `"`     | `&quot;` |



Per un riferimento completo sulla sintassi dei messaggi XML si rimanda alla documentazione ufficiale: [http://www.w3.org/TR/REC-xml/](http://www.w3.org/TR/REC-xml/).



### Identificazione della chiamata


La chiamata può essere identificata utilizzando diversi criteri, corrispondenti all'attributo `type` dell'elemento `<target>`:

*   `type="channelid"`: la chiamata viene identificata utilizzando l'identificativo univoco del canale. Ad esempio:

  `<target type="channelid">4QCHRLU7PT</target> `



*   `type="dn"`: la chiamata viene identificata utilizzando l'extension (o DN) univoca dell'operatore. Ad esempio:

  `<target type="dn">2100</target> `



*   `type="external_phone"`:la chiamata viene identificata utilizzando il numero di telefono,  (esterno) associato alla chiamata. Ad esempio:

  `<target type="external_phone">0248027177</target>`

  Per l'identificazione della chiamata tramite numero di telefono, è anche possibile utilizzare una sottostringa del numero di telefono, purchè funzionale alla corretta identificazione della chiamata. Ad esempio: 

  `<target type="external_phone">480271</target>`



*   `type="dtmf_sequence"`: la chiamata viene identificata utilizzando una sequenza di toni multifrequenza (DTMF) associata alla chiamata in corso. Ad esempio:

  `<target type="dtmf_sequence">#2100#</target> `

  La sequenza di toni DTMF deve essere generata dal Client sulla chiamata registrata prima di inviare la richiesta al servizio AIM Server. Si consiglia di utilizzare toni speciali per marcare inizio e fine della sequenza (come il carattere `#` nell'esempio precedente).

  Per l'identificazione della chiamata tramite toni DTMF, è anche possibile utilizzare una sottostringa della sequenza, purhè funzionale alla corretta identificazione della chiamata. Ad esempio:

  `<target type="dtmf_sequence">2100</target> `



*   `type="configured_ip"`: la chiamata viene identificata utilizzando l'indirizzo ip univoco con cui è stata configurata la postazione. Ad esempio:

  `<target type="configured_ip">192.168.18.7</target>`

  Il canale di registrazione viene identificato tramite l'associazione statica canale/indirizzo ip impostata nella configurazione dei canali di registrazione VoIP.


*   `type="configured_dn"`: la chiamata viene identificata utilizzando l'extension (DN) univoca con cui è stata configurata la postazione. Ad esempio:

  `<target type="configured_dn">2100</target>`

  Il canale di registrazione viene identificato tramite l'associazione statica canale/dn impostata nella configurazione dei canali di registrazione VoIP, oppure tramite l'associazione statica dn/indirizzo ip impostata in un file esterno. 

  A differenza di `type="dn"`, dove il valore dell'extension viene assegnata al canale dinamicamente quando la chiamata è in corso, in questo caso il relativo canale di registrazione viene associato staticamente.

  ​



**Nota**:

In alcuni scenari la chiamata oggetto della richiesta potrebbe non essere identificata: in questo caso AIM Server restituisce normalmente un errore con codice `1429` e descrizione `Unknown target`: in questo caso al Server non risulta alcuna registrazione associata al target impostato.

In altri scenari lo stesso valore target potrebbe essere associato a più chiamate contemporanee; in questo caso il Server restituisce un errore con codice `1455` e descrizione `"Multiple targets found"`; fa eccezione la richiesta [*GetChannelState*](#Messaggio *GetChannelState*), al quale, in caso di un valore associato a più di una chiamata, AIM Server risponde con l'elenco dei canali associati.



### Messaggio *StartRequest*


Con il messaggio *StartRequest* il Client richiede al Server l'avvio della registrazione di una chiamata (identificata dall'elemento `<target>`). Se il comando va a buon fine, la registrazione ha inizio contestualmente. 

Nello stesso messaggio è possibile inserire le informazioni con cui si vuole marcare la registrazione in corso, valorizzando opportunamente l'elemento `<tagging>`. Il alternativa è possibile isolare la richiesta di marcatura in un comando a parte, utilizzando la richiesta [*TagRequest*](#Messaggio *TagRequest*).

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*StartAnswer*](#Messaggio *StartAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo `type` deve assumere il valore `"request"`;
*   `<action>`: deve assumere il valore `"start"`;
*   `<target>`: il valore dell'elemento e l'attributo `type` devono essere valorizzati in maniera pertinente (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Gli elementi relativi alla marcatura della chiamata sono opzionali: 

*   `<tagging>`
    *   `<agent>`
    *   `<own_phone>`
    *   `<partner_phone>`
    *   `<call_direction>`
    *   `<comment>`
    *   `<external_callid>`
    *   `<ascii>`
    *   `<long>`

Tutti gli altri elementi non hanno significato nel contesto del messaggio.



**Esempi**

```
<message type="request" version="1.21">
	<action>start</action>
	<target type="dn">2100</target>
</message>
```
```
<message type="request" version="1.21">
	<action>start</action>
	<target type="dn">2100</target>
	<tagging>
		<agent>Mario Rossi</agent>
		<external_callid>8134567821</external_callid>
		<ascii index="2">servizio1</ascii>
	</tagging>
</message>
```



### Messaggio *StartAnswer*


Il messaggio *StartAnswer* viene inviato dal Server in risposta ad una richiesta di avvio registrazione ([Messaggio *StartRequest*](#Messaggio *StartRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo `type` assume il valore `"answer"`;
    *   `<action>`: assume il valore `"start"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;
    *   `<tagging>`: gli elementi relativi alla marcatura della chiamata (opzionali) vengono riportati esattamente come la richiesta relativa.

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
    *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
    *   `<description>`: è la descrizione del risultato;
    *   `<channel>`: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati;
        *   `<chstate>`: riporta lo stato del canale;
        *   `<callid>`: riporta l'identificativo univoco della chiamata;
        *   `<external_callid>`: riporta l'identificativo esterno della chiamata se valorizzato.

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:

* `<starttime>`: riporta data/ora di inizio della registrazione;

* `<own_phone>`, `<partner_phone>`, `<call_direction>`: riportano i numeri di telefono associati alla chiamata.

  ​



**Esempio**

```
<message type="answer" version="1.21">
	<action>start</action>
	<target type="dn">2100</target>
	<tagging>
		<agent>Mario Rossi</agent>
		<external_callid>8134567821</external_callid>
		<ascii index="2">servizio1</ascii>
    </tagging>
	<result>
		<code>0</code>
		<description>Command successfully executed (Tag request on target:
			2100)</description>
		<channel id=”4QCHRLU7PT”>
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid>8134567821</external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result>
</message>
```



### Messaggio *ForceStartRequest*


Con il messaggio *ForceStartRequest* il Client richiede al Server l'avvio della registrazione di una chiamata (identificata dall'elemento `<target>`). Se il comando va a buon fine, la registrazione ha inizio contestualmente.

Il comando è funzionalmente identico al messaggio [*StartRequest*](#Messaggio *StartRequest*), con l'unica differenza che la registrazione viene "forzata", ovvero viene avviata anche in assenza di una conversazione in corso.

Per una descizione completa si rimanda al paragrafo relativo a [*StartRequest*](#Messaggio *StartRequest*), con la sola differenza del parametro `<action>` che assume il valore `"forcestart"`;

Esempi

```
<message type="request" version="1.21">
	<action>forcestart</action>
	<target type="channelid">4QCHRLU7PT</target>
</message>
```

```
<message type="request" version="1.21">
	<action>forcestart</action>
	<target type="channelid">4QCHRLU7PT</target>
	<tagging>
		<agent>Mario Rossi</agent>
		<external_callid>8134567821</external_callid>
		<ascii index="2">servizio1</ascii>
	</tagging>
</message>
```





### Messaggio *ForceStartAnswer*

Il messaggio *ForceStartAnswer* viene inviato dal Server in risposta ad una richiesta di avvio registrazione forzata ([Messaggio *ForceStartRequest*](#Messaggio *StartRequest*)). Il messaggio è identico al messaggio [*StartAnswer*](#Messaggio *StartAnswer*), alla cui descrizione si rimanda, tranne che per il parametro `<action>`, che assume il valore `"forcestart"`.



**Esempio**

```
<message type="answer" version="1.21">
	<action>forcestart</action>
	<target type="channelid">4QCHRLU7PT</target>
	<tagging>
		<agent>Mario Rossi</agent>
		<external_callid>8134567821</external_callid>
		<ascii index="2">servizio1</ascii>
	</tagging>
	<result>
		<code>0</code>
		<description>Command successfully executed (Forcestart request on
			target: 2100)</description>
		<channel id=”4QCHRLU7PT”>
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid>8134567821</external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result>
</message>
```



### Messaggio *StopRequest*


Con il messaggio *StopRequest* il Client richiede al Server la chiusura della registrazione di una chiamata (identificata dall'elemento `<target>`). Se il comando va a buon fine, la registrazione termina contestualmente. 

Nello stesso messaggio è possibile inserire le informazioni con cui si vuole marcare la registrazione in corso, valorizzando opportunamente l'elemento `<tagging>`: in questo caso la marcatura viene effettuata immediatamente prima di chiudere la registrazione. Il alternativa è possibile isolare la richiesta di marcatura in un comando a parte (che deve essere inviato prima della richiesta di Stop), utilizzando la richiesta [*TagRequest*](#Messaggio *TagRequest*).

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*StopAnswer*](#Messaggio *StopAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo `type` deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"stop"`;
    *   `<target>`: il valore dell'elemento e l'attributo type devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Gli elementi relativi alla marcatura della chiamata sono opzionali: 


*   `<tagging>`
  *   `<agent>`
  *   `<own_phone>`
  *   `<partner_phone>`
  *   `<call_direction>`
  *   `<comment>`
  *   `<external_callid>`
  *   `<ascii>`
  *   `<long>`

Tutti gli altri elementi non hanno significato nel contesto del messaggio.

**Esempi**

```
<message type="request" version="1.21"> 
	<action>stop</action> 
	<target type="dn">2100</target> 
</message>
```
```
<message type="request" version="1.21"> 
	<action>stop</action> 
	<target type="dn">2100</target> 
	<tagging> 
		<agent>Mario Rossi</agent> 
		<external_callid>8134567821</external_callid> 
		<ascii index="2">servizio1</ascii> 
	</tagging> 
</message>
```



### Messaggio *StopAnswer*

Il messaggio *StopAnswer* viene inviato dal Server in risposta ad una richiesta di chiusura registrazione ([Messaggio *StopRequest*](#Messaggio *StopRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore "answer";
    *   `<action>`: assume il valore `"stop"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;
    *   `<tagging>`: gli elementi relativi alla marcatura della chiamata (opzionali) vengono riportati esattamente come la richiesta relativa.

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   <result>: è presente con i sottoelementi seguenti;
  *   <code>: assume un valore numerico relativo al risultato dell'operazione;
  *   <description>: è la descrizione del risultato;
  *   <channel>: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati;
    *   <chstate>: riporta lo stato del canale;
    *   <callid>: riporta l'identificativo univoco della chiamata;
    *   <external_callid>: riporta l'identificativo esterno della chiamata se valorizzato.

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:

*   <starttime>: riporta data/ora di inizio della registrazione;
  *   <own_phone>, <partner_phone>, <call_direction>: riportano i numeri di telefono associati alla chiamata.

**Esempio**

```
<message type="answer" version="1.21"> 
	<action>stop</action> 
	<target type="dn">2100</target> 
	<tagging> 
		<agent>Mario Rossi</agent> 
		<external_callid>8134567821</external_callid> 
		<ascii index="2">servizio1</ascii> 
	</tagging> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Stop request on target: 2100)</description> 
		<channel id="4QCHRLU7PT">
			<chstate>idle</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid>8134567821</external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result> 
</message>
```



### Messaggio *MuteRequest*

Con il messaggio *MuteRequest,* il Client richiede al Server di inserire del silenzio nella registrazione di una chiamata (identificata dall'elemento `<target>`). Se il comando va a buon fine, nella registrazione, avviata in precedenza, verrà inserito del silenzio fino al successivo richiesta di *UnMute* (paragrafo [TODO Link]) o alla chiusura della comunicazione.

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*MuteAnswer*](#Messaggio *MuteAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request";
    *   `<action>`: deve assumere il valore `"mute"`;
    *   `<target>`: il valore dell'elemento e l'attributo type devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Tutti gli altri elementi non hanno significato nel contesto del messaggio.

**Esempio**

```
<message type="request" version="1.21"> 
	<action>mute</action> 
	<target type="dn">2100</target> 
</message>
```



### Messaggio *MuteAnswer*

Il messaggio *MuteAnswer* viene inviato dal Server in risposta ad una richiesta di inserimento silenzio nella registrazione ([Messaggio *MuteRequest*](#Messaggio *MuteRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"mute"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
  *   `<description>`: è la descrizione del risultato;
  *   `<channel>`: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati;
    *   `<chstate>`: riporta lo stato del canale;
    *   `<callid>`: riporta l'identificativo univoco della chiamata;
        *   `<external_callid>`: riporta l'identificativo esterno della chiamata se valorizzato;

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:

*   <starttime>: riporta data/ora di inizio della registrazione;
*   <own_phone>, <partner_phone>, <call_direction>: riportano i numeri di telefono associati alla chiamata.



**Esempio**

```
<message type="answer" version="1.21"> 
	<action>mute</action> 
	<target type="dn">2100</target> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Mute request on target: 2100)</description> 
		<channel id="4QCHRLU7PT">
			<chstate>muted</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid></external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result> 
</message>
```



### Messaggio *UnMuteRequest*

Con il messaggio *UnMuteRequest* il Client richiede al Server di sospendere l'inserimento del silenzio nella registrazione di una chiamata (identificata dall'elemento `<target>`). Se il comando va a buon fine, nella registrazione, avviata e messa in mute in precedenza, riprenderà la normale registrazione dell'audio.

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*UnMuteAnswer*](#Messaggio *UnMuteAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `*"*request"`;
    *   `<action>`: deve assumere il valore `"unmute"`;
    *   `<target>`: il valore dell'elemento e l'attributo `type` devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Tutti gli altri elementi non hanno significato nel contesto del messaggio.



**Esempio**

```
<message type="request" version="1.21"> 
	<action>unmute</action> 
	<target type="dn">2100</target> 
</message>
```



### Messaggio *UnMuteAnswer*

Il messaggio *UnMuteAnswer* viene inviato dal Server in risposta ad una richiesta di sospensione di inserimento di silenzio nella registrazione (paragrafo [TODO Link]).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"unmute"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
  *   `<description>`: è la descrizione del risultato;
  *   `<channel>`: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati;
    *   `<chstate>`: riporta lo stato del canale;
    *   `<callid>`: riporta l'identificativo univoco della chiamata;
    *   `<external_callid>`: riporta l'identificativo esterno della chiamata se valorizzato;

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:

*   `<starttime>`: riporta data/ora di inizio della registrazione;
*   `<own_phone>`, `<partner_phone>`, `<call_direction>`: riportano i numeri di telefono associati alla chiamata.

**Esempio**

```
<message type="answer" version="1.21"> 
	<action>unmute</action> 
	<target type="dn">2100</target> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Unmute request on 				target: 2100)</description> 
		<channel id="4QCHRLU7PT">
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid></external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result> 
</message>
```



### Messaggio *KeepRequest*

Con il messaggio *KeepRequest* il Client richiede al Server il salvataggio di una registrazione di una chiamata (identificata dall'elemento `<target>`) in configurazione *SaveOnDemand*. Il canale di registrazione relativo deve essere opportunamente configurato (si veda il manuale di installazione del sistema di registrazione). Se il comando va a buon fine, la registrazione, avviata automaticamente in precedenza e marcata per la cancellazione, verrà correttamente salvata dal sistema di registrazione alla chiusura della comunicazione, a meno che non subentrino altri comandi *Delete* durante la chiamata stessa.

Nello stesso messaggio è possibile inserire le informazioni con cui si vuole marcare la registrazione in corso, valorizzando opportunamente l'elemento `<tagging>`. Il alternativa è possibile isolare la richiesta di marcatura in un comando a parte, utilizzando la richiesta [*TagRequest*](#Messaggio *TagRequest*).

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*KeepAnswer*](#Messaggio *KeepAnswer*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"keep"`;
    *   `<target>`: il valore dell'elemento e l'attributo type devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Gli elementi relativi alla marcatura della chiamata sono opzionali: 

*   `<tagging>`
  *   `<agent>`
    *   `<own_phone>`
    *   `<partner_phone>`
    *   `<call_direction>`
    *   `<comment>`
    *   `<external_callid>`
    *   `<ascii>`
    *   `<long>`

Tutti gli altri elementi non hanno significato nel contesto del messaggio.



**Esempio**

```
<message type="request" version="2.0"> 
	<action>keep</action> 
	<target type="dn">2100</target> 
	<tagging> 
		<agent>Mario Rossi</agent> 
		<external_callid>8134567821</external_callid> 
		<ascii index="2">servizio1</ascii> 
	</tagging> 
</message>
```



### Messaggio *KeepAnswer*

Il messaggio *KeepAnswer* viene inviato dal Server in risposta ad una richiesta di salvataggio registrazione ([Messaggio *KeepRequest*](#Messaggio *KeepRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"keep"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;
    *   `<tagging>`: gli elementi relativi alla marcatura della chiamata (opzionali) vengono riportati esattamente come la richiesta relativa.

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
    *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
    *   `<description>`: è la descrizione del risultato;
    *   `<channel>`: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati:
      *   `<chstate>`: riporta lo stato del canale;
        *   `<callid>`: riporta l'identificativo univoco della chiamata;
        *   `<external_callid>`: riporta l'identificativo esterno della chiamata se valorizzato;

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:
*   `<starttime>`: riporta data/ora di inizio della registrazione;
*   `<own_phone>`, `<partner_phone>`, `<call_direction>`: riportano i numeri di telefono associati alla chiamata.



**Esempio**

```
<message type="answer" version="1.21"> 
	<action>keep</action> 
	<target type="dn">2100</target> 
	<tagging> 
		<agent>Mario Rossi</agent> 
		<external_callid>8134567821</external_callid> 
		<ascii index="2">servizio1</ascii> 
	</tagging> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Keep request on target: 2100)</description> 
		<channel id="4QCHRLU7PT">
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid>8134567821</external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result> 
</message>
```



### Messaggio *DeleteRequest*

Con il messaggio *DeleteRequest* il Client richiede al Server di non salvare la registrazione di una chiamata (identificata dall'elemento `<target>`) in configurazione *SaveOnDemand*. Il canale di registrazione relativo deve essere opportunamente configurato (si veda il manuale di installazione del sistema di registrazione). Se il comando va a buon fine, la registrazione, avviata automaticamente in precedenza e marcata per il salvataggio, verrà scartata dal sistema di registrazione alla chiusura della comunicazione, a meno che non subentrino altri comandi *Keep* durante la chiamata stessa.

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*DeleteAnswer*](#Messaggio *DeleteAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"delete"`;
    *   `<target>`: il valore dell'elemento e l'attributo `type` devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Tutti gli altri elementi non hanno significato nel contesto del messaggio.

**Esempio**

```
<message type="request" version="1.21"> 
	<action>delete</action> 
	<target type="dn">2100</target> 
</message>
```



### Messaggio *DeleteAnswer*

Il messaggio *DeleteAnswer* viene inviato dal Server in risposta ad una richiesta di annullamento salvataggio registrazione ([Messaggio *DeleteRequest*](#Messaggio *DeleteRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo `type` assume il valore `"answer"`;
    *   `<action>`: assume il valore `"delete"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
  *   `<description>`: è la descrizione del risultato;
  *   `<channel>`: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati;
    *   `<chstate>`: riporta lo stato del canale;
            *   `<callid>`: riporta l'identificativo univoco della chiamata;
            *   `<external_callid>`: riporta l'identificativo esterno della chiamata se valorizzato;

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:

*   `<starttime>`: riporta data/ora di inizio della registrazione;
*   `<own_phone>`, `<partner_phone>`, `<call_direction>`: riportano i numeri di telefono associati alla chiamata.

**Esempio**

```
<message type="answer" version="1.21"> 
	<action>delete</action> 
	<target type="dn">2100</target> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Delete request on 			target: 2100)</description> 
		<channel id="4QCHRLU7PT">
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid></external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result> 
</message>
```



### Messaggio *TagRequest*

Con il messaggio *TagRequest* il Client richiede al Server di associare alcune informazioni alla registrazione in corso. In alternativa l'operazione di marcatura può essere effettuata congiuntamente ai comandi [*StartRequest*](#Messaggio *StartRequest*), [*ForceStart*](#Messaggio *ForceStartRequest*), [*StopRequest*](#Messaggio *StopRequest*) e [*KeepRequest*](#Messaggio *KeepRequest*). Se il comando va a buon fine, la registrazione viene marcata con le informazioni riportate nella richiesta.

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*TagAnswer*](#Messaggio *TagAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"tag"`;
    *   `<target>`: il valore dell'elemento e l'attributo `type` devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Gli elementi relativi alla marcatura della chiamata sono opzionali: 

*   `<tagging>`
  *   `<agent>`
  *   `<own_phone>`
  *   `<partner_phone>`
    *   `<call_direction>`
    *   `<comment>`
    *   `<external_callid>`
    *   `<ascii>`
    *   `<long>`

Tutti gli altri elementi non hanno significato nel contesto del messaggio.

**Esempio**

```
<message type="request" version="2.0"> 
	<action>tag</action> 
	<target type="dn">2100</target> 
	<tagging> 
		<agent>Mario Rossi</agent> 
		<external_callid>8134567821</external_callid> 
		<ascii index="2">servizio1</ascii> 
	</tagging> 
</message>
```



### Messaggio *TagAnswer*

Il messaggio *TagAnswer* viene inviato dal Server in risposta ad una richiesta di marcatura registrazione ([*TagRequest*](#Messaggio *TagRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"tag"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;
    *   `<tagging>`: gli elementi relativi alla marcatura della chiamata (opzionali) vengono riportati esattamente come la richiesta relativa.

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
    *   `<description>`: è la descrizione del risultato;
    *   `<channel>`: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati;
    *   <chstate>: riporta lo stato del canale;
        *   <callid>: riporta l'identificativo univoco della chiamata;
        *   <external_callid>: riporta l'identificativo esterno della chiamata se valorizzato;

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:

*   `<starttime>`: riporta data/ora di inizio della registrazione;
*   `<own_phone>`, `<partner_phone>`, `<call_direction>`: riportano i numeri di telefono associati alla chiamata.

**Esempio**

```
<message type="answer" version="2.0"> 
	<action>tag</action> 
	<target type="dn">2100</target> 
	<tagging> 
		<agent>Mario Rossi</agent> 
		<external_callid>8134567821</external_callid> 
		<ascii index="2">servizio1</ascii> 
	</tagging> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Tag request on 				target: 2100)</description>
		<channel id="4QCHRLU7PT">
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid></external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result> 
</message>
```



### Messaggio *GetChStateRequest*

Con il messaggio *GetChStateRequest* il Client richiede al Server lo stato di uno specifico canale, identificato dall'elemento `<target>`. Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*GetChStateAnswer*](#Messaggio *GetChStateAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"getchstate"`;
    *   `<target>`: il valore dell'elemento e l'attributo `type` devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata));

Tutti gli altri elementi non hanno significato nel contesto del messaggio.

**Esempio**

```
<message type="request" version="1.21"> 
	<action>getchstate</action> 
	<target type="dn">2100</target> 
</message>
```



### Messaggio *GetChStateAnswer*

Il messaggio *GetChStateAnswer* viene inviato dal Server in risposta ad una richiesta di stato su uno specifico canale ([Messaggio *GetChStateRequest*](#Messaggio *GetChStateRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"getchstate"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
    *   `<description>`: è la descrizione del risultato;
    *   `<channel>`: se la chiamata è correttamente identificata, l'attributo id riporta l'identificativo del canale, ed i sottovalori seguenti sono valorizzati;
      *   `<chstate>`: riporta lo stato del canale;
        *   `<callid>`: riporta l'identificativo univoco della chiamata;
        *   `<external_callid>`: riporta l'identificativo esterno della chiamata se valorizzato;

I seguenti campi (se abilitati in configurazione, e se valorizzati) riportano inoltre i dettagli della chiamata in corso:

*   `<starttime>`: riporta data/ora di inizio della registrazione;
*   `<own_phone>`, `<partner_phone>`, `<call_direction>`: riportano i numeri di telefono associati alla chiamata.

Come già accennato nel paragrafo [Identificazione della chiamata](#Identificazione della chiamata), in caso di un valore associato a più di una chiamata, AIM Server risponde con l'elenco dei canali associati. Il client è responsabile per la corretta gestione di tali scenari.



**Esempi**

```
<message type="answer" version="1.21"> 
	<action>getchstate</action> 
	<target type="dn">2100</target> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Get channel state request on target: 2100)</description>
		<channel id="4QCHRLU7PT">
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid></external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
	</result> 
</message>
```
```
<message type="answer" version="2.0"> 
	<action>getchstate</action> 
	<target type="dn">2100</target> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Get channel state request on target: 2100)</description>
		<channel id="4QCHRLU7PT">
			<chstate>recording</chstate>
			<callid>0E5XJ9X5O4QCHRLU7PT</callid>
			<external_callid></external_callid>
			<starttime>2009-07-17 09:50:04</starttime>
		</channel>
		<channel id="4QCHRLU7Q4">
			<chstate>recording</chstate>
			<callid>0E5XJAA6O4QCHRLU7Q4</callid>
			<external_callid></external_callid>
			<starttime>2009-07-17 09:50:14</starttime>
		</channel>
	</result> 
</message>
```



### Messaggio *CheckExtRequest*

Con il messaggio *CheckExtRequest* il Client richiede al Server se una specifica Extension è configurata per l'utilizzo con il recorder, ovvero se un canale è staticamente associato ad una determinata Extension. 

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*CheckExtAnswer*](#Messaggio *CheckExtAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"checkext"`;
    *   `<target>`: il valore dell'elemento e l'attributo `type` devono essere valorizzati in maniera pertinente  (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata)); l'attributo `type` in questo contesto può assumere indistintamente i valori `"dn"` oppure `"configured_dn"`.

Tutti gli altri elementi non hanno significato nel contesto del messaggio.



**Esempio**

```
<message type="request" version="2.0"> 
	<action>checkext</action> 
	<target type="dn">2100</target> 
</message>
```



**Nota**

AIM Server può controllare per i canali VoIP la configurazione statica delle extension e degli indirizzi ip all'avvio del servizio e giornalmente ad un orario prefissato.  In questo modo vengono ricaricate le impostazioni in caso di modifica alla destinazione dei canali.

Inoltre AIM può controllare ad intervalli configurabili un file esterno su cui è riportata l'associazione statica indirizzo ip/extension; tale file esterno deve essere popolato a partire dal sistema telefonico. L'utilizzo di questa mappatura esterna, congiuntamente ad una configurazione dei canali VoIP per indirizzo ip, rende possibile una associazione di tipo *free seating* con *extension mobility*, in cui gli operatori associano la propria extension alla postazione telefonica tramite login.

Nel caso di aggiunta o eliminazione di canali, affinchè AIM prenda in carico le modifiche è necessario riavviare AIM (dopo avere eventualmente aggiornato anche la configurazione dei canali) per fare in modo che le modifiche vengano prese in carico.

Per la configurazione della lettura della configurazione statica da file di configurazione (`recman.ini.xml`) e da file esterno, si rimanda al paragrafo [Configurazione di AIM Server](#Configurazione di AIM Server).



### Messaggio *CheckExtAnswer*

Il messaggio *CheckExtAnswer* viene inviato dal Server in risposta ad una verifica di configurazione  per Extension ([Messaggio *CheckExtRequest*](#Messaggio *CheckExtRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"checkext"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
  *   `<description>`: è la descrizione del risultato;



**Esempio**

```
<message type="answer" version="2.0"> 
	<action>checkext</action> 
	<target type="dn">2100</target> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Check Extension request on target: 2100)</description>
	</result> 
</message>
```



### Messaggio *CheckIPRequest*

Con il messaggio *CheckIpRequest* il Client richiede al Server se un canale è staticamente associato ad uno specifico indirizzo ip. 

Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*CheckIpAnswer*](Messaggio *CheckIpAnswer*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"checkip"`;
    *   `<target>`: il valore dell'elemento e l'attributo `type` devono essere valorizzati in maniera pertinente (vedere il paragrafo [Identificazione della chiamata](#Identificazione della chiamata)); l'unico valore consentito per l'attributo `type` in questo contesto è `"configured_ip"`.

Tutti gli altri elementi non hanno significato nel contesto del messaggio.

**Nota**

Si vedano le considerazioni al precendente paragrafo [Messaggio *CheckExtRequest*](Messaggio *CheckExtRequest*).

**Esempio**

```
<message type="request" version="1.21"> 
	<action>checkip</action> 
	<target type="configured_ip">192.168.100.22</target> 
</message>
```



### Messaggio *CheckIpAnswer*

Il messaggio *CheckIpAnswer* viene inviato dal Server in risposta ad una verifica di configurazione per indirizzo ip ([Messaggio *CheckIPRequest*](#Messaggio *CheckIPRequest*)).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"checkip"`;
    *   `<target>`: vengono riportati i valori della richiesta relativa;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
  *   `<description>`: è la descrizione del risultato;



**Esempio**

```
<message type="answer" version="2.0"> 
	<action>checkext</action> 
	<target type="configured_ip">192.168.100.22</target> 
	<result> 
		<code>0</code> 
		<description>Command successfully executed (Check IP request on target: 192.168.100.22)</description>
	</result> 
</message>
```



### Messaggio *IsAliveRequest*

Con il messaggio *IsAliveRequest* il Client richiede al Server lo stato di servizio. Il Server notifica al Client il risultato della richiesta con il messaggio relativo [*IsAliveAnswer*](Messaggio *IsAliveAnswer*). 

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo type deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"isalive"`;

Tutti gli altri elementi non hanno significato nel contesto del messaggio.



**Esempio**

```
<message type="request" version="1.21"> 
	<action>isalive</action> 
</message>
```



### Messaggio *IsAliveAnswer*

Il messaggio *IsAliveAnswer* viene inviato dal Server in risposta ad una richiesta di stato [*IsAliveRequest*](Messaggio *IsAliveRequest*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo type assume il valore `"answer"`;
    *   `<action>`: assume il valore `"isalive"`;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
    *   `<description>`: è la descrizione del risultato; contiene anche la data del Server.



**Nota**

A differenza del messaggio [*WatchdogRequest*](#Messaggio *WatchdogRequest*) che effettua controlli aggiuntivi, con questo test vengono verificati solamente la raggiungibilità del Server e la disponibilità del servizio (protocollo di comandi).



**Esempio**

```
<message type="answer" version="2.0"> 
	<action>isalive</action> 
	<result> 
		<code>0</code> 
		<description>AIM is alive (time: 2013/12/10 18:00:27)</description> 
	</result> 
</message>

```



### Messaggio *WatchdogRequest*

Con il messaggio *WatchdogRequest* il Watchdog richiede al Server lo stato generale del Server. Il Server notifica al Watchdog il risultato della richiesta con il messaggio relativo [*WatchdogAnswer*](#Messaggio *WatchdogAnswer*). 

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML). In particolare, è necessario valorizzare gli elementi:

*   `<message>`: l'attributo `type` deve assumere il valore `"request"`;
    *   `<action>`: deve assumere il valore `"watchdog"`;

Tutti gli altri elementi non hanno significato nel contesto del messaggio.

**Nota**

Il messaggio è normalmente utilizzato da AIM Watchdog e non deve essere utilizzato dai client connessi.

**Esempio**

```
<message type="request" version="2.0"> 
	<action>watchdog</action> 
</message>

```



### Messaggio *WatchdogAnswer*

Il messaggio *WatchdogAnswer* viene inviato dal Server in risposta ad una richiesta [*WatchdogRequest*](#Messaggio *WatchdogRequest*).

Il messaggio è formattato secondo le specifiche riportate al paragrafo [Formato del messaggio XML](#Formato del messaggio XML).  In particolare, i seguenti elementi sono valorizzati:

*   `<message>`: l'attributo `type` assume il valore `"answer"`;
    *   `<action>`: assume il valore `"watchdog"`;

I seguenti tag vengono contestualizzati con il risultato dell'operazione:

*   `<result>`: è presente con i sottoelementi seguenti;
  *   `<code>`: assume un valore numerico relativo al risultato dell'operazione;
    *   `<description>`: è la descrizione del risultato; contiene anche la data del Server.



**Nota**

Come nel caso del messaggio [*IsAliveRequest*](#Messaggio *IsAliveRequest*), vengono verificati la raggiungibilità del Server e la disponibilità del servizio (protocollo di comandi).

Se l'opzione *EnableInternalDispatcherCheck* è attiva (si veda il paragrafo [Configurazione di AIM Server](#Configurazione di AIM Server)), alla ricezione del messaggio [*WatchdogRequest*](#Messaggio *WatchdogRequest*) AIM Server esegue anche un test interno di comunicazione con IAS API Server. 



**Esempi**

```
<message type="answer" version="2.0"> 
	<action>watchdog</action> 
	<result> 
		<code>0</code> 
		<description>Protocol and dispatcher threads are alive (time: 2013/12/10 18:00:27)</description> 
	</result> 
</message>

```
```
<message type="answer" version="2.0"> 
	<action>watchdog</action> 
	<result> 
		<code>0</code> 
		<description>Protocol threads are alive (time: 2013/12/10 18:00:27)</description> 
	</result> 
</message>

```







# Funzionalità opzionali

## AIM Watchdog

AIM Watchdog, già brevemente descritto nel paragrafo [Architettura e specifiche funzionali](#Architettura e specifiche funzionali), è uno strumento utile a verificare che AIM Server sia attivo sulla porta desiderata e che funzioni in modo regolare. AIM Watchdog è installato come servizio sulla stessa macchina su cui è attivo AIM Server. Per i dettagli di configurazione, si veda il paragrafo [Configurazione di AIM Server](#Configurazione di AIM Server).

AIM Watchdog ad intervalli regolari si occupa di inviare al server una richiesta [*WatchdogRequest*](#Messaggio *WatchdogRequest*). e di controllare la risposta del Server. In caso di ripetuti failure, il Watchdog può richiamare uno script esterno (configurabile) che può essere utilizzato per riavviare AIM Server (operazione di default) oppure effettuare una qualsiasi operazione prevista per la gestione dell'evento.

**Nota**: con AIM Watchdog viene monitorato sia il funzionamento dell'interfaccia verso i client (AIMProtocol) che il funzionamento interno dell'interfaccia verso il modulo APIServer interno al recorder: infatti ad ogni richiesta del Watchdog viene effettuata una verifica interna con invio di un comando *dummy* verso API Server, la cui mancata risposta evidenzia un problema interno. 



## Funzione *AutoRestart*


In alcuni scenari le registrazioni possono durare molto a lungo (anche più giorni). 

Per facilitare la gestione di queste chiamate, è possibile abilitare la funzione *AutoRestart*, mediante la quale i canali vengono riavviati in automatico da un thread dedicato di AIM Server allo scadere di un intervallo di tempo configurabile.

La funzione *AutoRestart* lavora su base canale: è possibile impostare per quali canali è attiva e per quali no. Per i dettagli di configurazione, si veda il paragrafo [Configurazione di AIM Server](#Configurazione di AIM Server).



## Invio di trap SNMP


Come già evidenziato al paragrafo [Architettura di monitoring](#Architettura di monitoring), è possibile integrare i servizi AIM con un sistema di monitoraggio predisposto per la ricezione di trap SNMP in formato *v2c*.

Le trap possono essere inviate sia da AIM Server che da AIM Watchdog; è inoltre possibile definire in modo capillare quali messaggi debbano essere inoltrati al sistema di monitoraggio e con quale livello (*error*, *warning*, *info*, *audit*): per maggiori dettagli sulla configurazione, si rimanda al paragrafo [Configurazione dell'invio di trap](#Configurazione dell'invio di trap).

Per ragioni di omogeneità, si è scelto di utilizzare lo stesso schema MIB delle trap inviate dal software ASC (si veda l'[Appendice B: Elenco delle trap SNMP configurabili](#Appendice B: Elenco delle trap SNMP configurabili) per la definizione completa). La tabella seguente mostra l'elenco delle variabili che descrivono la trap:



| Variabile                  | Tipo        | Nome               | Descrizione                              |
| -------------------------- | ----------- | ------------------ | ---------------------------------------- |
| .1.3.6.1.4.1.4063.2.1.2.1  | OctetString | evoSystemID        | ID del recorder                          |
| .1.3.6.1.4.1.4063.2.1.2.2  | OctetString | evoSystemName      | ID del sistema (hostname)                |
| .1.3.6.1.4.1.4063.2.1.2.3  | OctetString | evoModuleName      | Nome del modulo interno associato al messaggio |
| .1.3.6.1.4.1.4063.2.1.2.4  | OctetString | evoErrType         | Tipologia di messaggio (error, warning, info, audit) |
| .1.3.6.1.4.1.4063.2.1.2.5  | OctetString | evoErrCode         | ID messaggio                             |
| .1.3.6.1.4.1.4063.2.1.2.6  | OctetString | evoErrUniqueID     | ID univoco associato al messaggio        |
| .1.3.6.1.4.1.4063.2.1.2.7  | OctetString | evoErrOpenTime     | Datetime dell'errore espresso in localtime con il formato |
| .1.3.6.1.4.1.4063.2.1.2.8  | OctetString | evoErrUpdateTime   | Non utilizzato (impostato allo stesso valore di evoErrOpenTime) |
| .1.3.6.1.4.1.4063.2.1.2.9  | OctetString | evoErrCloseTime    | Non impostato (vuoto)                    |
| .1.3.6.1.4.1.4063.2.1.2.10 | OctetString | evoErrText         | Testo del messaggio                      |
| .1.3.6.1.4.1.4063.2.1.2.11 | OctetString | evoErrCloseComment | Non impostato (vuoto)                    |



**Nota**

La gestione eventi del software ASC prevede messaggi di due diverse tipologie:

*   Messaggi **persistent**: il sistema notifica un 'evento, ma non può controllarne lo stato. Il messaggio viene evidenziato su EventViewer, e deve essere chiuso manualmente da un'operatore.
*   Messaggi **non-persistent**: il sistema notifica un 'evento, di cui può controllarne lo stato. Il messaggio viene evidenziato su EventViewer come *open*, eventualmente come *updated*, ed infine come *closed* quando l'evento rientri.

I messaggi inviati da AIM appartengono tutti alla prima categoria. I campi *evoErrCloseTime* e *evoErrCloseComment*, mantenuti per compatibilità con il formato trap di ASC, di fatto non vengono mai valorizzati; il campo *evoErrUpdateTime* viene sempre valorizzato come *evoErrOpenTime*.



## AIM Monitor


A completamento delle funzionalità di monitoraggio già descritte al paragrafo [Architettura di monitoring](#Architettura di monitoring), è possibile utilizzare lo script AIM Monitor che permette di evidenziare lo stato dei due servizi attivi AIM e AIM Watchdog.

AIM Monitor ad intervalli regolari controlla lo stato dei due servizi:

*   nel caso in cui AIM Server fosse inattivo, AIM Monitor invia una trap al sistema di monitoraggio;
*   nel caso in cui AIM Watchdog fosse inattivo, AIM Monitor invia una trap al sistema di monitoraggio ed effettua un tentativo di riavvio di AIM Watchdog.

Di default AIM Monitor non è avviato. 

In ambiente Linux è possibile avviare manualmente il processo da shell Bash; eventualmente il processo può essere pianificato su *crontab* all'evento *@reboot.*

Su ambiente Windows è possibile avviare manualmente il processo da shell Command Prompt; eventualmente il processo può essere pianificato utilizzando il tool *Task Scheduler*.



**Nota**

AIM Monitor non sostituisce AIM Watchdog, che è in grado di interagire con Aim Server e di effettuare controlli più approfonditi sullo stato di funzionamento. Un eventuale riavvio di AIM Server viene effettuato dal Watchdog.



## AIM Shell Client


AIM Shell Client è un client minimale da linea di comando che permette di inviare richieste ad AIM Server direttamente da shell.

La sintassi del comando è la seguente:

```
java -Dlog4j.configurationFile=cfg/AIMShellClient.Log.xml -jar AIM.jar ShellClient [host] [port] '[request]' 
```

I parametri sono i seguenti:

*   `host`: l'indirizzo o nome host di AIM Server;
*   `port`: la porta su cui è configurato il servizio;
*   `request`: la richiesta xml secondo il protocollo AIM.



**Esempio**

```
java -Dlog4j.configurationFile=cfg/AIMShellClient.Log.xml -jar AIM.jar ShellClient 192.168.0.70 5055 '<message type="request" version="2.0"><action>isalive</action></message>'

```

AIM Shell Client, un a volta lanciato da shell con i comandi sopra indicati, si occupa di connettersi ad AIM Server, inviare il comando, attendere la risposta e infine chiudere la connessione.

I valori di ritorno del comando sono i seguenti:

*   0: il comando è andato a buon fine;

*   1: il Server ha restituito un errore. Per i dettagli dell'errore è necessario verificare i file di log.




## AIM Test

L'applicativo AIM Test permette di testare tutte le funzionalità del protocollo AIM. 



![AIM_Test.png](pics/AIM_Test.png)

*Figura 3: Applicazione AIM Test*



Nel dettaglio, con AIM Test è possibile:

*   Connettersi/disconnettersi ad un Server AIM;
*   Inviare periodiche richieste IsAlive per mantenere viva la connessione con il Server;
*   Effettuare test su tutti i comandi del protocollo AIM, incluse tutti i possibili parametri ed inviare i comandi al Server, e verificarne la risposta;
*   Visualizzare la sequenza di richieste e le relative risposte del server;
*   Visualizzare i log del client (AIM Test) e di AIM Server aggiornati in tempo reale.



**Nota**

E' possibile inviare al Server sia un comando singolo, sia una serie di comandi in sequenza; in quest'ultimo caso è sufficiente inserire più comandi nel pannello centrale prima di comandare l'invio con *Send*.





# Specifiche di installazione

## Ambienti supportati

Il software AIM può essere utilizzato sia su OS Linux che su OS Windows (con alcune piccole differenze relative all'installazione).

Il software è testato e rilasciato sui seguenti sistemi operativi:

**Linux**: SuSE Linux Enterprise Server ver. 10 e 11

**Windows**: Windows Server 2008 R2 - Windows Server 2012 R2

Il software è solitamente installato come modulo aggiuntivo direttamente sul recorder della famiglia ASC MARATHON (incluso EVOip Server), ma in alcuni progetti specifici può anche essere installato su una macchina esterna. 

Il software è testato e rilasciato per le seguenti versioni del software ASC: 

**MARATHON EVOLUTION/EVOlite ver. 10.x**

**EVOip Server ver. 10.x**

Prerequisiti: Oracle Java SE o JDK versione 8 o successiva.



## Distribuzione e installazione su Linux


Il software AIM è distribuito sotto forma di file compresso `aim_linux_v2.x.x.tar.gz` .

Una volta decompresso il file, per avviare l'installazione è necessario utilizzare lo script `AIM_linux_setup.sh`, che si occupa di:

*   creare directory e copiare i file necessari (il software AIM viene installato al percorso `/usr/adabus/aim`);
*   registrare i servizi AIM Server e AIM Watchdog per l'avvio automatico;
*   pianificare automaticamente (su *crontab*) lo script per la gestione dei log file.



**Nota**

Lo script di setup è valido per l'installazione standard come modulo aggiuntivo su un recorder ASC MARATHON o EVOip Server. Per altre tipologie di installazione è necessario modificare lo script o installare manualmente secondo necessità.

Nella tabella seguente è riportato l'elenco dei file installati:

| File                       | Path        | Note                                     |
| -------------------------- | ----------- | ---------------------------------------- |
| AIM.App.properties         | cfg         | File di configurazione (parametri AIM Server) |
| AIM.ChannelMap.properties  | cfg         | File di configurazione (mappa canali AIM Server) |
| AIM.Log.xml                | cfg         | File di configurazione (log AIM Server)  |
| AIM.SNMP.properties        | cfg         | File di configuraziont (trap SNMP)       |
| AIMWatchdog.App.properties | cfg         | File di configurazione (parametri AIM Watchdog) |
| AIMWatchdog.Log.xml        | cfg         | File di configurazione (log AIM Watchdog) |
| AIMShellClient.Log.xml     | cfg         | File di configurazione (log AIM Shell Client) |
| AIMTest.App.properties     | cfg         | File di configurazione (parametri AIM Test) |
| AIMTest.Log.xml            | cfg         | File di configurazione (log AIM Test)    |
| startAIM                   | .           | Script per l'avvio di AIM Server         |
| stopAIM                    | .           | Script per la chiusura di AIM Server     |
| startWATCH                 | .           | Script per l'avvio di AIM Watchdog       |
| stopWATCH                  | .           | Script per la chiusura di AIM Watchdog   |
| restartAIM                 | .           | Script per il riavvio di AIM Server      |
| monitorAIM                 | .           | Script per il monitoraggio dei servizi AIM |
| logman                     | .           | Script per la gestione dei log file      |
| AIMv2.jar                  | .           | Libreria principale software AIM         |
| iasapi.jar                 | lib         | Libreria utilizzata da AIM Server (ASC S&R API Java) |
| log4j-api-2.9.1.jar        | lib         | Libreria terze parti per il logging degli applicativi |
| log4j-core-2.9.1.jar       | lib         | Libreria terze parti per il logging degli applicativi |
| snmp4j-2.5.8.jar           | lib         | Libreria terze parti per l'invio di trap snmp |
| xom-1.2.10.jar             | lib         | Libreria terze parti per il parsing XML  |
| AdabusServices             | /etc/init.d | Script di avvio servizi                  |
| log                        | .           | Link al percorso /calldata/aimlog dove vengono salvati i file di log |



Nella distribuzione è presente anche lo script `AIM_linux_remove.sh`, che può essere utilizzato per la rimozione completa di AIM dal sistema.



**Attenzione**

Il setup si occupa solamente della parte sistemistica. La configurazione deve essere effettuata manualmente in seguito, modificando i file nella cartella `cfg` (si veda il paragrafo [Configurazione di AIM Server](#Configurazione di AIM Server)).



## Distribuzione e installazione su Windows


Il software AIM è distribuito sotto forma di file compresso `aim_win_v2.x.x.zip` .

Una volta decompresso il file, per avviare l'installazione è necessario utilizzare lo script `AIM_win_setup.bat`, che si occupa di:

*   creare directory e copiare i file necessari, in due directory separate per AIM Server e AIM  Watchdog (rispettivamente su `%ProgramFiles%\Adabus\aim` e `%ProgramFiles%\Adabus\aimwatchdog`);
*   registrare i servizi AIM Server e AIM Watchdog per l'avvio automatico; i servizi sono riportati sulla console di amministrazione servizi (services.msc) con i rispettivi nomi *Adabus AIM Server v2*  e *Adabus AIM Watchdog v2*;
*   pianificare automaticamente (Scheduled Tasks) lo script per la gestione dei log file.




**Nota**

Lo script di setup è valido per l'installazione standard come modulo aggiuntivo su un recorder ASC  EVOip Server. Per altre tipologie di installazione è necessario modificare lo script.

Nella tabella seguente è riportato l'elenco dei file installati al percorso `aim`:

| File                      | Path | Note                                     |
| ------------------------- | ---- | ---------------------------------------- |
| AIM.App.properties        | cfg  | File di configurazione (parametri AIM Server) |
| AIM.ChannelMap.properties | cfg  | File di configurazione (mappa canali AIM Server) |
| AIM.Log.xml               | cfg  | File di configurazione (log AIM Server)  |
| AIM.SNMP.properties       | cfg  | File di configuraziont (trap SNMP)       |
| AIMShellClient.Log.xml    | cfg  | File di configurazione (log AIM Shell Client) |
| AIMTest.App.properties    | cfg  | File di configurazione (parametri AIM Test) |
| AIMTest.Log.xml           | cfg  | File di configurazione (log AIM Test)    |
| logman.bat                | .    | Script per la gestione dei log file      |
| monitorAIM.bat            | .    | Script per il monitoraggio dei servizi AIM |
| sv_install.bat            | .    | Script per l'installazione del servizio AIM Server |
| sv_uninstall.bat          | .    | Script per la disinstallazione del servizio AIM Server |
| prunmgr.exe               | .    | Eseguibile per l'applicazione di terze parti Apache Windows Service Wrapper (procrun) |
| prunsrv.exe               | .    | Eseguibile per l'applicazione di terze parti Apache Windows Service Wrapper (procrun) |
| AIMv2.jar                 | .    | Libreria principale software AIM         |
| iasapi.jar                | lib  | Libreria utilizzata da AIM Server (ASC S&R API Java) |
| log4j-api-2.9.1.jar       | lib  | Libreria terze parti per il logging degli applicativi |
| log4j-core-2.9.1.jar      | lib  | Libreria terze parti per il logging degli applicativi |
| snmp4j-2.5.8.jar          | lib  | Libreria terze parti per l'invio di trap snmp |
| xom-1.2.10.jar            | lib  | Libreria terze parti per il parsing XML  |



Nella tabella seguente è riportato l'elenco dei file installati al percorso `aimwatchdog`:

| File                       | Path | Note                                     |
| -------------------------- | ---- | ---------------------------------------- |
| AIMWatchdog.App.properties | cfg  | File di configurazione (parametri AIM Watchdog) |
| AIMWatchdog.Log.xml        | cfg  | File di configurazione (log AIM Server)  |
| AIM.SNMP.properties        | cfg  | File di configuraziont (trap SNMP)       |
| sv_install.bat             | .    | Script per l'installazione del servizio AIM Watchdog |
| sv_uninstall.bat           | .    | Script per la disinstallazione del servizio AIM Watchdog |
| restart.bat                | .    | Script per il riavvio di AIM Server      |
| prunmgr.exe                | .    | Eseguibile per l'applicazione di terze parti Apache Windows Service Wrapper (procrun) |
| prunsrv.exe                | .    | Eseguibile per l'applicazione di terze parti Apache Windows Service Wrapper (procrun) |
| AIMv2.jar                  | .    | Libreria principale software AIM         |
| iasapi.jar                 | lib  | Libreria utilizzata da AIM Server (ASC S&R API Java) |
| log4j-api-2.9.1.jar        | lib  | Libreria terze parti per il logging degli applicativi |
| log4j-core-2.9.1.jar       | lib  | Libreria terze parti per il logging degli applicativi |
| snmp4j-2.5.8.jar           | lib  | Libreria terze parti per l'invio di trap snmp |
| xom-1.2.10.jar             | lib  | Libreria terze parti per il parsing XML  |



Nella distribuzione è presente anche lo script *AIM_win_remove.bat*, che può essere utilizzato per la rimozione completa di AIM dal sistema.



**Attenzione**

Il setup si occupa solamente della parte sistemistica. La configurazione deve essere effettuata manualmente in seguito, modificandoi file nella cartella `cfg` (si veda il paragrafo [Configurazione di AIM Server](#Configurazione di AIM Server)).



## Configurazione di AIM Server


Per configurare AIM Server è necessario modificare i seguenti file:

*   *AIM.App.properties*
*   *AIM.ChannelMap.properties*
*   *AIM.Log.xml*



Di seguito sono riportati i parametri di configurazione del file *AIM.App.properties*:

| Parametro                            | Note                                     |
| ------------------------------------ | ---------------------------------------- |
| MarathonUser                         | Utenza applicativa di AIM Server per l'accesso al recorder via ASC S&R API Java (default: *aim_user*) |
| MarathonPassword                     | La password dell'utenza applicativa MarathonUser |
| APIServerIP                          | Indirizzo dell'API Server (Recorder) connesso all'AIM Server |
| APIServerPort                        | La porta dell'API Server connesso (default: *4000*) |
| ExternalCallIDField                  | Il campo interno al database in cui viene mappato il parametro external_callid del protocollo AIM (default: *ASCII1*) |
| CommandWaitTimeout                   | Timeout in millisecondi per l'attesa di risposta ai comandi dell'API Server (default: *5000*) |
| ServerIP                             | IP Address per il protocollo AIM         |
| ListenPort                           | La porta relativa al protocollo AIM (default: *5055*) |
| SocketTimeout                        | Timeout in millisecondi dopo il quale un socket aperto ed inattivo viene chiuso automaticamente dal Server (default: *120000*) |
| ServerToServerMode                   | Abilita la modalità server/server (default: *off*) |
| WorkerThreads                        | Numero di Worker Threads per singola connessione in modalità server/server (default: *10*) |
| EnableAutoRestart                    | Abilita la funzione "AutoRestart" (default: *false*) |
| AutoRestartInterval                  | Intervallo (in sec.) per la funzione "Autorestart" (default: *120*) |
| AutoRestartInfoField                 | Il campo interno al database in cui vengono salvate le informazioni di "AutoRestart" (default: *ASCII20*) |
| EnableRecmanConfigurationCheck       | Abilita la lettura delle configurazioni statiche dei canali (extension e ip address) dal file *recman.ini.xml* (file di configurazione del modulo RecordingManager) (default: *false*) |
| EnableRecmanConfigurationRefresh     | Abilita la rilettura periodica delle configurazioni statiche dei canali da file di configurazione (default: *false*) |
| RecmanConfigurationRefreshTime       | Orario in cui effettuare la rilettura periodica delle configurazioni statiche dei canali (default: *21:00*) |
| RecmanIni                            | Il percorso dove trovare il file recman.ini.xml (default: */usr/asc/bin/recman/recman.ini.xml*) |
| EnableExternalConfigurationCheck     | Abilita la lettura delle configurazioni statiche dei canali (dn e ip address) da un file esterno in formato *csv* (*dn,ip*) (default: *false*) |
| EnableExternalConfigurationRefresh   | Abilita la rilettura periodica delle configurazioni statiche dei canali da file esterno (default: *false*) |
| ExternalConfigurationRefreshInterval | Intervallo in secondi per la rilettura delle configurazioni statiche da file esterno (default: *60*). |
| ExternalConfigurationFile            | Il percorso dove trovare il file di configurazione esterno. |
| EnableStarttimeReturnInfo            | Abilita il ritorno del tag  nelle risposte del server AIM (default: *true*) |
| EnableStorageStateReturnInfo         | Abilita il ritorno del tag  nelle risposte del server AIM (default: *false*) |
| EnableCallDetailsReturnInfo          | Abilita il ritorno dei tag , ,  nelle risposte del server AIM (default: *false*) |
| EnableThirdPartyPhoneReturnInfo      | Abilita il ritorno del tag  nelle risposte del server AIM (default: *false*) |
| EnableInternalDispatcherCheck        | Abilita il check interno del modulo "IAS API Event Dispatcher" (default: *true*) |
| EnableInternalAPILogging             | Abilita il log interno aggiuntivo in formato ASC (default: *false*) |



Di seguito sono riportati i parametri di configurazione del file *AIM.ChannelMap.properties*:

| Parametro                | Note                                     |
| ------------------------ | ---------------------------------------- |
| DefineRange              | Flag (*yes/no*) per definire uno o più range di canali controllati via AIM Server (default: *no*) |
| RangeNo                  | Numero di range definiti (default: *1*)  |
| Range\<N>_FirstChannelID | Il primo ChannelID di un range di canali controllati via AIM Server. A seconda del valore RangeNo, per ogni range previsto deve essere valorizzato il FirstChannelID (es.: *Range1_FirstChannelID*, *Range2_FirstChannelID*, etc...) |
| Range\<N>_RangeSize      | Dimensione del range di canali. A seconda del valore RangeNo, per ogni range previsto deve essere valorizzato il parametro  RangeSize (es.: *Range_RangeSize*, *Range2_RangeSize*, etc...) |
| DefineMap                | Flag (*yes/no*) per definire un elenco di canali controllati via AIM Server (default: *no*) |
| \<channelid>             | Singolo elemento dell'elenco di canali controllati via AIM Server, da abilitare (*on*) o disabilitare (*off*) |
| DefineAutorestartable    | Flag (*yes/no*) per definire un elenco di canali per cui è impostata la funzione "AutoRestart" (default: *no*) |
| autorestart_\<channelid> | Singolo elemento dell'elenco di canali per cui è impostata la funzione "*AutoRestart*", da abilitare (*on*) o disabilitare (*off*) |




Per la configurazione dei file di log è necessario modificare *AIM.Log.xml*; si rimanda al paragrafo [Configurazione file di log](#Configurazione file di log).

Per la configurazione opzionale dell'invio di trap SNMP si rimanda al paragrafo [Configurazione dell'invio di trap](#Configurazione dell'invio di trap).



## Configurazione di AIM Watchdog


Per configurare AIM Watchdog è necessario modificare i seguenti file:

*   *AIMWatchdog.App.properties*

*   *AIMWatchdog.Log.xml*

    ​

Di seguito sono riportati i parametri di configurazione del file *AIMWatchdog.App.properties*:



| Parametro        | Note                                     |
| ---------------- | ---------------------------------------- |
| ServerIP         | IP Address per il protocollo AIM         |
| ListenPort       | La porta relativa al protocollo AIM (default: *5055*) |
| Max Errors       | Numero massimo di errori consecutivi. Al raggiungimento del valore il Watchdog fa partire lo script configurato in CriticalAction (default: *5*) |
| NextCheckTimeout | Intervallo di tempo tra due test eseguiti dal Watchdog sul Server (default: *10000*) |
| SocketTimeout    | Timeout in millisecondi per la connessione del Watchdog verso il Server (default: *10000*) |
| CriticalAction   | Script che viene avviato dal Watchdog in caso di fault ripetuti (default: */usr/adabus/aim/restartAIM*) |



Per la configurazione dei file di log è necessario modificare *AIMWatchdog.Log.xml*. Si rimanda al paragrafo [Configurazione file di log](#Configurazione file di log).

Per la configurazione opzionale dell'invio di trap SNMP si rimanda al paragrafo [Configurazione dell'invio di trap](#Configurazione dell'invio di trap).





## Configurazione dell'invio di trap 

La configurazione dell'invio di trap SNMP in AIM Server e AIMWatchdog è definita interamente nel file  AIM.SNMP.properties. Di seguito sono riportati i parametri di configurazione del file:



| Parametro         | Note                                     |
| ----------------- | ---------------------------------------- |
| EnableSNMPtraps   | Abilita l'invio delle trap SNMP (default: *false*). |
| ReceiverIP        | Indirizzo IP dell'SNMP Node Manager che riceve le trap inviate dai servizi AIM Server e AIM Watchdog. |
| ReceiverPort      | Porta su cui l'SNMP Node Manager rimane in attesa per la ricezione delle trap (default: *162*). |
| SNMPCheckInterval | Intervallo di tempo (in ms.) tra due attività di invio di trap (default: *1000*) |
| RecorderID        | Identificativo numerico univoco del recorder, da riportare nelle trap inviate. |
| snmp_\<messageid> | Abilitazione del singolo messaggio  all'invio come trap SNMP; si veda la nota seguente per la definizione della sintassi. |



**Nota**

La sintassi del campo `snmp_<messageid>` prevede la definizione di due parametri separati da virgola:

```
snmp_<messageid>=<TrapID>,<TrapType>
```

ove

* `TrapID`: identificativo univoco del messaggio

* `TrapType`: la tipologia di messaggio (SNMP_ERROR, SNMP_WARNING, SNMP_INFO, SNMP_AUDIT)

  ​

**Esempio**:

```
snmp_MarathonStartCallError=AIM_MARATHON_MANAGER_START_ERROR,SNMP_INFO
```



Per una lista completa dei messaggi si rimanda all'[Appendice B: Elenco delle trap SNMP configurabili](#Appendice B: Elenco delle trap SNMP configurabili). Per facilitare la configurazione dei messaggi inviati, nel file *AIM.SNMP.properties* è riportata la lista completa dei messaggi. Per abilitare l'invio delle trap relativo a uno o più messaggi, è sufficiente decommentare le righe relative.



## Configurazione di AIM Shell Client


La configurazione di AIM Shell Client si limita al file AIMShellClient.Log.xml. Si rimanda al paragrafo [Configurazione file di log](#Configurazione file di log).



## Configurazione di AIM Test


Per configurare AIM Test è necessario modificare i seguenti file:

*   *AIMTest.App.properties*
*   *AIMTest.Log.xml*


Di seguito sono riportati i parametri di configurazione del file *AIMTest.App.properties*:



| Parametro        | Note                                     |
| ---------------- | ---------------------------------------- |
| ServerLogPath    | Percorso del file di log di AIM Server   |
| ClientLogPath    | Percorso del file di log di AIM Test     |
| EnableServerLog  | Abilita la visualizzazione del log di AIM Server |
| LogCheckInterval | Intervallo di tempo (in sec.) per l'aggiornamento del file di log (default: *2*) |
| IsAliveInterval  | Intervallo (in sec.) tra due richiestedi IsAlive da AIM Test a AIM Server (default: *60*) |



Per la configurazione dei file di log è necessario modificare *AIMTest.Log.xml*. Si rimanda al paragrafo [Configurazione file di log](#Configurazione file di log).





##Configurazione file di log


Gli applicativi AIM Server, AIM Watchdog, AIM Shell Client e AIM Test registrano la propria attività in modo dettagliato nei file di log relativi:

*   *AIM.log*
*   *AIMWatchdog.log*
*   *AIMShellClient.log*
*   *AIMTest.log*



AIM Server ha anche un log interno secondario (*AIMAPI.log*), che può essere attivato utilizzando il parametro `EnableInternalAPILogging` (si veda [Configurazione di AIM Server](#configurazione-di-AIM-Server)).

I file sono salvati di default nella sottocartella `log`. Nel caso di Linux, `log` è in realtà un link, per cui i file si trovano fisicamente al percorso `/calldata/aimlog`. In ambiente Windows i file sono invece salvati nella sottocartella (e dunque nella partizione primaria); è comunque possibile modificare il percorso come riportato più avanti.

La struttura interna dei file di log è la seguente:

```
YYYY-MM-DD HH:MM:SS,mmm | message type | ThreadID | Module | message
```

I file sono configurati di default per essere storicizzati con cadenza giornaliera. A fine giornata viene dunque creato un file di backup con l'indicazione della data nel formato `<file>.<yyyy-mm-dd>` (ad esempio *AIM.log.2013-12-12*).

Per ogni applicazione è presente un file per la configurazione del log:

*   AIM.Log.xml
*   AIMWatchdog.Log.xml
*   AIMShellClient.Log.xml
*   AIMTest.Log.xml


Il software utilizza la libreria *Apache Log4j 2*, in caso di necessità è necessario consultare la [documentazione ufficiale](https://logging.apache.org/log4j/2.x/manual/configuration.html). I valori di default dei parametri sono già adatti ad ogni tipo di installazione, si consiglia dunque di non modificarli. Meritano però attenzione i seguenti attributi dei *RollingFile Appenders*:



| Parametro             | Note                                     |
| --------------------- | ---------------------------------------- |
| filePattern="PATTERN" | Con questo parametro è possibile modificare il periodo di storicizzazione dei file di log. Ad esempio: storicizzazione giornaliera (default): `filePattern="${log-path}/AIM_%d{yyyy-MM-dd}.log"`, storicizzazione oraria: `filePattern="${log-path}/AIM_%d{yyyy-MM-dd_HH}.log"` |
| filename="FILENAME"   | Con questo parametro è possibile modificare il percorso ed il nome del file di log. Ad esempio (default): `fileName="${log-path}/AIM.log"` |



Poichè i log generati dai servizi AIM Server e AIM Watchdog possono raggiungere in produzione dimensioni considerevoli, in fase di installazione viene automaticamente pianificato l'avvio giornaliero di uno script per la manutenzione dei file di log.

In ambiente Linux, lo script utilizzato è `logman`, che si occupa di comprimere (in formato gzip) i file di log chiusi da più di 1 giorno e di cancellare i file storici più vecchi di 30 giorni. Lo script può essere facilmente modificato e adattato a diverse esigenze di progetto.

Lo script è pianificato di default per l'avvio giornaliero alle 21:00. Per modificare la pianificazione è sufficiente modificare il file di sistema `/etc/crontab`.

In ambiente Windows, lo script utilizzato è `logman.bat`, che si occupa di cancellare i file storici più vecchi di 30 giorni. Lo script può essere facilmente modificato e adattato a diverse esigenze di progetto.

Anche in questo caso lo script è pianificato per l'avvio giornaliero alle 21:00; la pianificazione può essere modificata utilizzando lo strumento Task Scheduler di Windows.





## Avvio delle applicazioni


Gli applicativi AIM Server, AIM Watchdog sono installati sui recorder come servizi con avvio automatico all'avvio di sistema, sia in ambiente Linux che in ambiente Windows.

In caso fosse necessario riavviarli, in ambiente Linux è possibile utilizzare gli script presenti nel percorso di installazione:

*   `./startAIM` e `./stopAIM` (per il controllo di AIM Server) 
*   `./startWATCH` e `./stopWATCH` (per il controllo di AIM Watchdog)



In ambiente Windows i servizi possono essere controllati tramite console di gestione  servizi (*services.msc*).

In alternativa, è possibile utilizzare i seguenti comandi direttamente da shell:

```
java -Dlog4j.configurationFile=cfg/AIM.Log.xml -jar AIM.jar Server start
```
```
java -Dlog4j.configurationFile=cfg/AIMWatchdog.Log.xml -jar AIM.jar Watchdog start
```

Per l'utilizzo dell'applicativo AIM ShellClient, si veda il paragrafo relativo [AIM Shell Client](#AIM Shell Client) 

Per quanto riguarda infine AIM Test, esso va utilizzato normalmente su una macchina separata, opportunamente configurato. 

È possibile avviare l'applicazione di test utilizzando il comando seguente da shell:

```
java -Dlog4j.configurationFile=cfg/AIMTest.Log.xml -jar AIM.jar Test
```







# Scenari di integrazione

In questa sezione sono descritti in dettaglio a titolo esemplificativo alcuni scenari di integrazione, comprensivi di buone pratiche per un utilizzo ottimale.



## Esempi di architetture applicative


Il software AIM si presta ad essere utilizzato in molteplici ambiti. In questa sezione verranno esaminate nel dettaglio alcune architetture di massima al fine di dare un'indicazione sull'utilizzo reale sul campo dell'integrazione con AIM Server.

Di seguito è riportato l'elenco degli scenari illustrati:

*   [**Controllo Start/Stop e marcatura con registrazione "passiva"**](#Esempio: controllo Start/Stop e marcatura con registrazione "passiva"): registrazione basata su sniffing dei pacchetti voce di telefoni IP (oppure su tecnologia TDM). Tramite integrazione con AIM viene effettuato il controllo della registrazione (*Record on Demand*) e la marcatura con dati di contesto.
*   [**Controllo Keep/Delete e marcatura con registrazione "passiva"**](#Esempio: controllo Keep/Delete e marcatura con registrazione "passiva"): registrazione basata su sniffing dei pacchetti voce di telefoni IP (oppure su tecnologia TDM). Tramite integrazione con AIM viene effettuato il controllo della registrazione (*Save on Demand*) e la marcatura con dati di contesto.
*   [**Marcatura con registrazione "attiva"**](#Esempio: Marcatura con registrazione "attiva"): registrazione basata su conferenza IP (o conferenza TDM). Tramite integrazione con AIM viene effettuata la marcatura con dati di contesto.
*   [**Scenari ridondati**](#Esempio: Registrazione ridondata): la registrazione (attiva o passiva) viene effettuata in parallelo su due diversi sistemi di registrazione. L'integrazione deve prevedere l'invio degli stessi comandi a due AIM Server.

Infine, al paragrafo [Note](#Note) vengono riportati alcune note di implementazione relative agli scenari descritti.





## Esempio: controllo Start/Stop e marcatura con registrazione "passiva"


In questo scenario di esempio si ha l'esigenza di registrare le chiamate di un ContactCenter. La modalità di registrazione è *RecordOnDemand*: la chiamata viene registrata solo in seguito ad una richiesta specifica di Start da parte del software di integrazione CTI Client (barra telefonica).

Il seguente diagramma mostra un'architettura di esempio:



![architettura_esempio_1.png](pics/architettura_esempio_1.png)

*Figura 4: Architettura AIM Server con registrazione passiva*



Come già descritto al paragrafo [Specifiche di comunicazione (modalità client/server)](#Specifiche di comunicazione (modalità client/server)), il Server rimane in ascolto su una specifica porta *tcp* in attesa delle connessioni dei Client. La chiusura della connessione è normalmente demandata al Client; il Server si occupa invece di chiudere le connessioni rimaste inattive per un certo periodo (parametro configurabile), al fine di liberare spazio e risorse sul Server.

La modalità ottimale di comunicazione tra Client e Server è la seguente:

1.  Il Client, all'avvio del software, od in seguito a login dell'operatore, effettua una connessione TCP sulla porta in attesa.
2.  Il Server gestisce questa nuova connessione con un *thread* dedicato.
3.  Il Client invia ad intervalli regolari un messaggio di *IsAliveRequest* al fine di mantenere aperta la connessione ed avere una notifica continua sullo stato di servizio del Server. L'intervallo tra una richiesta e l'altra deve comunque essere inferiore del timeout per la chiusura della connessione impostato sul Server.
4.  Il Server risponde con *IsAliveAnswer.*
5.  Ogni qual volta è prevista la registrazione della chiamata (tramite innesco automatico o manuale), il Client invia al Server una richiesta *GetChStateRequest* per verificare lo stato del canale di registrazione. 
6.  Il Server risponde con il relativo messaggio *GetChStateAnswer*. Per poter effettuare la registrazione con successo, è necessario che il Server indichi che lo stato del canale è *active* (fonia presente).
7.  Il Client invia una *StartRequest* comprensiva di dati di marcatura.
8.  Il Server risponde con il relativo messaggio *StartAnswer*.


1.  Il Client può proseguire con la normale operatività, inviando i messaggi *IsAliveRequest o GetChStateRequest*.
2.  Il Server risponde con *IsAliveAnswer* o *GetChStateAnswer.*
3.  (opzione) La registrazione può essere interrotta tramite un comando esplicito *StopRequest* – la conversazione continua regolarmente ma non è registrata.
4.  (opzione) Il Server risponde con il relativo messaggio *StopAnswer*.
5.  (opzione) La chiamata termina con registrazione in corso. La registrazione viene chiusa in automatico dal sistema di registrazione.
6.  Alla disconnessione di un operatore, o alla chiusura del software, il Client può chiudere la connessione.
7.  Il Server libera le risorse associate alla connessione.

​
## Esempio: controllo Keep/Delete e marcatura con registrazione "passiva"

In questo scenario di esempio si ha l'esigenza di registrare le chiamate di un ContactCenter. La modalità di registrazione è *SaveOnDemand*: la chiamata è normalmente registratata in automatico dal sistema fin dall'inizio della conversazione, ma la registrazione viene scartata a meno che sopraggiunga una richiesta specifica di Keep da parte del software di integrazione CTI Client (barra telefonica). 

L'architettura di esempio è la stessa riportata al precedente paragrafo [Controllo Start/Stop e marcatura con registrazione "passiva"](#Esempio: controllo Start/Stop e marcatura con registrazione "passiva").

Come già descritto al paragrafo [Specifiche di comunicazione (modalità client/server)](#Specifiche di comunicazione (modalità client/server)), il Server rimane in ascolto su una specifica porta *tcp* in attesa delle connessioni dei Client. La chiusura della connessione è normalmente demandata al Client; il Server si occupa invece di chiudere le connessioni rimaste inattive per un certo periodo (parametro configurabile), al fine di liberare spazio e risorse sul Server.

La modalità ottimale di comunicazione tra Client e Server è la seguente:

1.  Il Client, all'avvio del software, od in seguito a login dell'operatore, effettua una connessione TCP sulla porta in attesa.
2.  Il Server gestisce questa nuova connessione con un *thread* dedicato.
3.  Il Client invia ad intervalli regolari un messaggio di *IsAliveRequest* al fine di mantenere aperta la connessione ed avere una notifica continua sullo stato di servizio del Server. L'intervallo tra una richiesta e l'altra deve comunque essere inferiore del timeout per la chiusura della connessione impostato sul Server.
4.  Il Server risponde con *IsAliveAnswer*.
5.  Ogni qual volta è previsto il salvataggio della registrazione, il Client invia al Server una richiesta *GetChStateRequest* per verificare lo stato del canale di registrazione. 
6.  Il Server risponde con il relativo messaggio *GetChStateAnswer*. In questo contesto (*Save On Demand*), è necessario che il Server indichi che lo stato del canale è *recording* (fonia presente e registrazione attiva).
7.  Il Client invia un *KeepRequest*  comprensiva di dati di marcatura.
8.  Il Server risponde con il relativo messaggio *KeepAnswer*.
9.  Il Client può proseguire con la normale operatività, inviando i messaggi *IsAliveRequest o GetChStateRequest*.
10.  Il Server risponde con *IsAliveAnswer* o *GetChStateAnswer*. 
11.  (opzione) Il salvataggio può essere annullato tramite un comando esplicito *DeleteRequest* la registrazione continua regolarmente ma alla chiusura della chiamata, non verrà salvata. 
12.  (opzione) Il Server risponde con il relativo messaggio *DeleteAnswer*.
13.  (opzione) La chiamata termina. La registrazione viene chiusa in automatico dal sistema di registrazione; avendo effettuato un precedente comando *Keep*, la registrazione viene salvata. 
14.  Alla disconnessione di un operatore, o alla chiusura del software, il Client può chiudere la connessione.
15.  Il Server libera le risorse associate alla connessione.




## Esempio: Marcatura con registrazione "attiva"

In questo scenario di esempio si ha l'esigenza di registrare le chiamate di un ContactCenter. La modalità di registrazione è *attiva*: il recorder è parte attiva del sistema telefonico e viene chiamato in conferenza dall'operatore al fine di registrare la conversazione.

L'architettura di esempio è riportata di seguito:



![architettura_esempio_3.png](pics/architettura_esempio_3.png)

*Figura 5: Architettura AIM Server con registrazione attiva*



Come già descritto al paragrafo [Specifiche di comunicazione (modalità client/server)](#Specifiche di comunicazione (modalità client/server)), il Server rimane in ascolto su una specifica porta *tcp* in attesa delle connessioni dei Client. La chiusura della connessione è normalmente demandata al Client; il Server si occupa invece di chiudere le connessioni rimaste inattive per un certo periodo (parametro configurabile), al fine di liberare spazio e risorse sul Server.

La modalità ottimale di comunicazione tra Client e Server è la seguente:

1.  Il Client, all'avvio del software, od in seguito a login dell'operatore, effettua una connessione TCP sulla porta in attesa.
2.  Il Server gestisce questa nuova connessione con un *thread* dedicato.
3.  Il Client invia ad intervalli regolari un messaggio di *IsAliveRequest* al fine di mantenere aperta la connessione ed avere una notifica continua sullo stato di servizio del Server. L'intervallo tra una richiesta e l'altra deve comunque essere inferiore del timeout per la chiusura della connessione impostato sul Server.
4.  Il Server risponde con *IsAliveAnswer.*
5.  Ogni qual volta è prevista la registrazione della chiamata (tramite innesco automatico o manuale), il Client invia al Server una richiesta *GetChStateRequest* per verificare lo stato del canale di registrazione. 
6.  Il Server risponde con il relativo messaggio *GetChStateAnswer*. Per poter effettuare la registrazione con successo, è necessario che il Server indichi lo stato del canale come *idle* (nessuna fonia presente). 
7.  Nel caso ci fossero registrazioni "appese" non desiderate (stato del canale: *recording*), il Client può inviare un messaggio *StopRequest* e attendere la relativa risposta *StopAnswer.*
8.  L'operatore effettua una conferenza telefonica verso il sistema di registrazione, la registrazione ha inizio.
9.  Il Client invia un comando *TagRequest* contenente i dati di marcatura da associare alla registrazione.
10.  Il Server risponde con *IsAliveAnswer.*
11.  Il Client può proseguire con la normale operatività, inviando i messaggi *IsAliveRequest* *oppure *GetChStateRequest* per tenere monitorato lo stato del canale.
12.  Il Server risponde con *IsAliveAnswer* o *GetStateAnswer*.
13.  (opzione) La registrazione può essere interrotta tramite un comando esplicito *StopRequest* – la conversazione continua regolarmente, ma  il ramo della conferenza verso il recorder viene chiuso e la registrazione termina.
14.  (opzione) Il Server risponde con il relativo messaggio *StopAnswer*.
15.  (opzione) La chiamata termina con registrazione in corso. La registrazione viene chiusa in automatico dal sistema di registrazione.
16.  Alla disconnessione di un operatore, o alla chiusura del software, il Client può chiudere la connessione.
17.  Il Server libera le risorse associate alla connessione.




## Esempio: Registrazione ridondata

Nei precedenti paragrafi sono stai descritti alcuni scenari con registrazione passiva o attiva, riferiti sempre ad architetture con un singolo sistema di registrazione.

Molto spesso è invece necessario disporre di un sistema completamente ridondato, in cui la registrazione avvenga contemporaneamente su due diversi sistemi.

Nel caso della registrazione *passiva*, la ridondanza si ottiene mettendo due sistemi di registrazione identici, che prelevano i pacchetti da span port.

Ogni registratore è equipaggiato con il proprio AIM Server ed è completamente indipendente; il  Client deve dunque inviare gli stessi comandi entrambi i recorder.

La gestione dei risultati può variare molto a seconda del contesto: ad esempio in molti casi potrebbe essere sufficiente per il client verificare che almeno uno dei recorder abbia risposto correttamente.

Si veda ad esempio il seguente diagramma (simile agli esempi riportati al paragrafo [Esempio: controllo Start/Stop e marcatura con registrazione "passiva"](#Esempio: controllo Start/Stop e marcatura con registrazione "passiva"), ma con la registrazione ridondata):



![architettura_esempio_ridondata1.png](pics/architettura_esempio_ridondata1.png)



*Figura 6: Architettura AIM Server con registrazione ridondata passiva*



Nel caso della registrazione *attiva*, la ridondanza si ottiene invece mettendo due sistemi di registrazione identici, che vengono ingaggiati in una conferenza a 4.

Si veda ad esempio il seguente diagramma (simile all'esempio riportato al [Esempio: Marcatura con registrazione "attiva"](#Esempio: Marcatura con registrazione "attiva"), ma con la registrazione ridondata):



![architettura_esempio_ridondata2.png](pics/architettura_esempio_ridondata2.png)



*Figura 7: Architettura AIM Server con registrazione ridondata attiva*



Anche in questo caso ogni registratore è equipaggiato con il proprio AIM Server ed è completamente indipendente. 

In questo caso il Client deve instaurare una conferenza a 4 per poter effettuare una registrazione ridondata; inoltre, analogamente al caso della registrazione passiva ridondata, dovrà inviare gli stessi comandi entrambi i recorder.

La gestione dei risultati può variare molto a seconda del contesto: ad esempio in molti casi potrebbe essere sufficiente per il client verificare che almeno uno dei recorder abbia risposto correttamente.

In uno scenario di registrazione attiva ridondata, non è possibile terminare in automatico la registrazione della conferenza a 4 semplicemente chiudendo la conversazione (come descritto al punto 15 del paragrafo [Esempio: Marcatura con registrazione "attiva"](#Esempio: Marcatura con registrazione "attiva") con registrazione non ridondata); in questo caso è invece sempre necessario inviare ai recorder il comando di stop esplicito (con comando [*StopRequest*](#Messaggio *StopRequest*)): infatti nel contesto della conferenza a 4, senza un comando di stop esplicito, la registrazione rimarrebbe attiva tra i due recorder. 



## Note


Di seguito sono riportate alcune importanti note relative all'implementazione degli scenari descritti:

1.  In caso di chiusura del socket inaspettata, il Client deve gestire una riconnessione con il Server. 
2.  La modalità ottimale prevede una connessione sempre attiva per ogni Client connesso; il Client ha comunque la facoltà di aprire una nuova connessione per ogni richiesta verso il Server. Questa modalità di interazione non è da preferire in quanto vengono utilizzati un gran numero di socket che possono portare in alcuni casi a saturazione delle risorse disponibili. Inoltre, lato Server c'è un carico maggiore per la continua apertura/chiusura di socket.
3.  Data la natura asincrona della comunicazione tra il Client, il Server ed il modulo API Server del recorder, è necessario che il Client attenda la risposta del Server prima di ripetere più volte la stessa richiesta. In caso di richieste simultanee relative allo stesso canale, il Server è costretto a serializzare le richieste; in particolare, è possibile che le condizioni di buona esecuzione di un comando vengano meno: in questo caso il Server risponde con un errore con codice `1467` e descrizione `Inconsistent request after wait`.






# Appendici

## Appendice A: Metodi di esempio per la comunicazione client/server

Il seguente codice (in linguaggio Java) mostra due metodi di esempio per la lettura e la scrittura corretta sul socket per AIM protocol. Come si evince dal codice seguente, è necessario effettuare due operazioni di lettura (*read*): la prima lettura (di 4 bytes) è necessaria per poter decodificare la lunghezza del messaggio; con la seconda lettura viene effettivamente recuperato il messaggio (testo in xml).

```java
// receive a Protocol message
private Ret ReceiveMsg(ProtocolMessage message)
{
	byte sizeBuffer[];
	byte msgBuffer[];

  	//receive message
	try {
		//read buffer
		sizeBuffer = new byte[4];
		inStream.read(sizeBuffer, 0, 4);
		//get CallDirection
		ByteBuffer byteBuffer = ByteBuffer.wrap(sizeBuffer, 0, 4);
		//sets byte ordering for numeric values
		byteBuffer.order(ByteOrder.BIG_ENDIAN);
		//get value
		Integer messageSize = byteBuffer.getInt();
		//log
		manager.Log(LogLevel.INFO, LogMsg.ProtocolNewRequestReceived, "client: " +
			clientSocket.getInetAddress().getHostAddress() +
			" - message size: " + messageSize.toString());
		//read remaining part of the message
		msgBuffer = new byte[messageSize];
		inStream.read(msgBuffer, 0, messageSize);
	} catch(IOException e) {
		//io error
		manager.Log(LogLevel.ERROR, LogMsg.ProtocolIOError, e.getMessage());
		return Ret.ERROR;
	}
  
	//reads XML string
	String xmlMessage;
	try {
		xmlMessage = new String(msgBuffer, "ISO-8859-1");
		//LogMsg
		manager.Log(LogLevel.INFO, LogMsg.ProtocolReceivedMessage, xmlMessage);
    } catch(UnsupportedEncodingException ex) {
		//parse error - encoding
		manager.Log(LogLevel.ERROR, LogMsg.ProtocolParseErrorEncoding, ex.getMessage());
		return Ret.ERROR;
	}

  	//parse request
	return message.ParseProtocolMessage(xmlMessage, manager);
}
```

```java
// send answer back
private Ret SendAnswer(ProtocolMessage message)
{
	//log
	manager.Log(LogLevel.INFO, LogMsg.ProtocolSendAnswer);
	//prepare buffer
  	String xmlMessage = message.toXML();
	int totalSize = 4 + xmlMessage.length();
	ByteBuffer buffer = ByteBuffer.allocate(totalSize);
	buffer.order(ByteOrder.BIG_ENDIAN);
	buffer.putInt(xmlMessage.length());
	try {
		buffer.put(xmlMessage.getBytes("ISO-8859-1"));
	} catch (UnsupportedEncodingException e) {
		manager.Log(LogLevel.ERROR, LogMsg.ProtocolParseErrorEncoding, e.getMessage());
	}
	//write
  	try {
		//write buffer
		outStream.write(buffer.array(), 0, totalSize);
		manager.Log(LogLevel.INFO, LogMsg.ProtocolAnswerSent, xmlMessage);
	} catch(IOException e) {
		//io error
		manager.Log(LogLevel.ERROR, LogMsg.ProtocolIOError, e.getMessage());
		return Ret.ERROR;
	}
	
  	//done
	return Ret.OK;
}
```





## Appendice B: Elenco delle trap SNMP configurabili


La seguente tabella mostra l'elenco dei messaggi configurabili per l'invio come trap SNMP:



| InternalID                            | TrapID                                   | Type         | Descrizione                              |
| ------------------------------------- | ---------------------------------------- | ------------ | ---------------------------------------- |
| ModuleCloseFailed                     | AIM_MODULE_CLOSE_FAILED                  | SNMP_WARNING | Messaggio interno.                       |
| ModuleAlreadyInitialized              | AIM_MODULE_ALREADY_INITIALIZED           | SNMP_WARNING | Messaggio interno.                       |
| ModuleNotInitialized                  | AIM_MODULE_NOT_INITIALIZED               | SNMP_WARNING | Messaggio interno.                       |
| ModuleClosed                          | AIM_MODULE_CLOSED                        | SNMP_INFO    | Messaggio interno.                       |
| AppGo                                 | AIM_APP_RUNNING                          | SNMP_INFO    | AIM Server è attivo. Messaggio inviato ad intervalli regolari durante il normale funzionamento. |
| AppCannotReadConfig                   | AIM_APP_CANNOT_READ_CONFIG               | SNMP_ERROR   | Errore relativo alla configurazione.     |
| AppCannotReadChannelMap               | AIM_APP_CANNOT_READ_CHANNELMAP           | SNMP_ERROR   | Errore relativo alla configurazione.     |
| AppMarathonManagerInitialized         | AIM_APP_MARATHON_MANAGER_INITIALIZED     | SNMP_INFO    | Inizializzazione del modulo Marathon Manager completata. |
| AppProtocolManagerInitialized         | AIM_APP_PROTOCOL_MANAGER_INITIALIZED     | SNMP_INFO    | Inizializzazione del modulo Protocol Manager completata. |
| AppInterrupted                        | AIM_APP_INTERRUPTED                      | SNMP_INFO    | L'esecuzione di AIM Server è stata interrotta. |
| AppCannotReadRecmanConfiguration      | AIM_APP_CANNOT_READ_RECMAN_CONFIG        | SNMP_WARNING | Errore relativo alla configurazione.     |
| AppCannotReadSNMPConfig               | AIM_APP_CANNOT_READ_SNMP_CONFIG          | SNMP_WARNING | Errore relativo alla configurazione.     |
| AppSNMPSenderInitialized              | AIM_APP_SNMP_SENDER_INITIALIZED          | SNMP_INFO    | Inizializzazione del modulo SNMP Manager completata. |
| AppRefreshRecmanConfiguration         | AIM_APP_REFRESH_RECMAN_CONFIGURATION     | SNMP_INFO    | La configurazione statica dei canali viene riletta. |
| AppCannotReadExternalConfiguration    | AIM_APP_CANNOT_READ_EXTERNAL_CONFIG      | SNMP_WARNING | Errore relativo alla configurazione.     |
| AppRefreshExternalConfiguration       | AIM_APP_REFRESH_EXTERNAL_CONFIGURATION   | SNMP_INFO    | La configurazione statica dei canali viene riletta. |
| AppRecmanConfigurationRefreshed       | AIM_APP_RECMAN_CONFIGURATION_REFRESHED   | SNMP_INFO    | La configurazione statica dei canali è stata riletta. |
| AppExternalConfigurationRefreshed     | AIM_APP_EXTERNAL_CONFIGURATION_REFRESHED | SNMP_INFO    | La configurazione statica dei canali è stata riletta. |
| ConfigPropsFileError                  | AIM_CONFIG_PROPERTY_FILE_ERROR           | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigPropsWrongVersion               | AIM_CONFIG_PROPERTY_WRONG_VERSION        | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigPropsWrongProduct               | AIM_CONFIG_PROPERTY_WRONG_PRODUCT        | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigValue                           | AIM_CONFIG_VALUE                         | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigDefaultValue                    | AIM_CONFIG_DEFAULT_VALUE                 | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigDefaultUsed                     | AIM_CONFIG_DEFAULT_USED                  | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigInvalidValue                    | AIM_CONFIG_INVALID_VALUE                 | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigChannelAddedToMap               | AIM_CONFIG_CHANNEL_ADDED                 | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigChannelRemovedFromMap           | AIM_CONFIG_CHANNEL_REMOVED               | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigAutorestartChannelNotMapped     | AIM_CONFIG_AUTORESTART_CHANNEL_NOT_ MAPPED | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigAutorestartChannelAdded         | AIM_CONFIG_AUTORESTART_CHANNEL_ADDED     | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigRecmanIniParseErrorValidity     | AIM_CONFIG_RECMAN_ERROR_VALIDITY         | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigRecmanIniParseErrorXML          | AIM_CONFIG_RECMAN_ERROR_XML              | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigRecmanIniParseErrorIO           | AIM_CONFIG_RECMAN_ERROR_IO               | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigRecmanIniParseErrorChannelID    | AIM_CONFIG_RECMAN_ERROR_CHANNEL          | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigRecmanParseError                | AIM_CONFIG_RECMAN_PARSE_ERROR            | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigRecmanDisabledChannel           | AIM_CONFIG_RECMAN_DISABLED_CHANNEL       | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigRecmanIniCheckChannel           | AIM_CONFIG_RECMAN_CHECK_CHANNEL          | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigRecmanNoStaticDataTDM           | AIM_CONFIG_RECMAN_NO_STATIC_DATA_TDM     | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigRecmanStaticDataMapped          | AIM_CONFIG_RECMAN_STATIC_DATA_MAPPED     | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigRecmanNoStaticData              | AIM_CONFIG_RECMAN_NO_STATIC_DATA         | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigSNMPParseError                  | AIM_CONFIG_SNMP_PARSE_ERROR              | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigSNMPTrapConfigured              | AIM_CONFIG_SNMP_TRAP_CONFIGURED          | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigStaticRefreshTimeParseError     | AIM_CONFIG_STATIC_REFRESH_TIME_PARSE_ERROR | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigEmptyChannelMap                 | AIM_CONFIG_EMPTY_CHANNEL_MAP             | SNMP_ERROR   | Errore relativo alla configurazione.     |
| ConfigExternalIPNotMapped             | AIM_CONFIG_EXTERNAL_IP_NOT_MAPPED        | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigExternalException               | AIM_CONFIG_READ_EXTERNAL_FILE_ERROR      | SNMP_WARNING | Errore relativo alla configurazione.     |
| ConfigExternalDNtoIPMapped            | AIM_CONFIG_EXTERNAL_DN_TO_IP_MAPPED      | SNMP_INFO    | Dettaglio di configurazione.             |
| ConfigExternalSyncing                 | AIM_CONFIG_EXTERNAL_SYNCING_DN           | SNMP_INFO    | Avvio sincronizzazione mappatura dati esterni. |
| ConfigExternalSyncDN                  | AIM_CONFIG_EXTERNAL_DN_SYNCED            | SNMP_INFO    | Sincronizzazione mappatura dati esterni terminata. |
| MarathonEventReceived                 | AIM_MARATHON_EVENT_RECEIVED              | SNMP_INFO    | Evento ricevuto da API Server.           |
| MarathonConnectionStateUnknown        | AIM_MARATHON_UNKNOWN_CONNECTION_STATE    | SNMP_WARNING | Stato della connessione non riconosciuto. |
| MarathonResultUnknown                 | AIM_MARATHON_UNKNOWN_RESULT              | SNMP_WARNING | Risultato di un comando non riconosciuto. |
| MarathonAPIConfigurationReceived      | AIM_API_CONFIGURATION_RECEIVED           | SNMP_INFO    | Configuration Event ricevuto da API Server. |
| MarathonClientConnected               | AIM_MARATHON_CLIENT_CONNECTED            | SNMP_INFO    | AIM Server è connesso a API Server.      |
| MarathonManagerStarting               | AIM_MARATHON_MANAGER_STARTING            | SNMP_INFO    | Il modulo Marathon Manager è in fase di avvio. |
| MarathonClientLoginFailed             | AIM_MARATHON_MANAGER_LOGIN_FAILED        | SNMP_WARNING | Login ad API Server fallito.             |
| MarathonClientConnectionFailed        | AIM_MARATHON_MANAGER_CONNECTION_FAILED   | SNMP_WARNING | Connessione ad API Server fallita.       |
| MarathonWrongAPIServerIP              | AIM_MARATHON_MANAGER_WRONG_APISERVER_ IP | SNMP_ERROR   | Indirizzo IP di API Server erroneo.      |
| MarathonCommandSent                   | AIM_MARATHON_MANAGER_COMMAND_SENT        | SNMP_INFO    | Comando inviato                          |
| MarathonClientLoggedIn                | AIM_MARATHON_MANAGER_CLIENT_LOGGED_IN    | SNMP_INFO    | Modulo Marathon Manager correttamente loggato su API Server. |
| MarathonClientDisconnected            | AIM_MARATHON_MANAGER_CLIENT_DISCONNECTED | SNMP_INFO    | Modulo Marathon Manager disconnesso da API Server. |
| MarathonClientLoggedOut               | AIM_MARATHON_MANAGER_CLIENT_LOGGED_OUT   | SNMP_INFO    | Modulo Marathon Manager non loggato su API Server. |
| MarathonClientLogoutFailed            | AIM_MARATHON_MANAGER_CLIENT_LOGOUT_ FAILED | SNMP_INFO    | Errore durante il logout di Marathon Manager da API Server. |
| MarathonMonitoringActive              | AIM_MARATHON_MANAGER_MONITORING_ACTIVE   | SNMP_INFO    | Il monitoring degli eventi è attivo      |
| MarathonMonitoringFailed              | AIM_MARATHON_MANAGER_MONITORING_FAILED   | SNMP_ERROR   | Il monitoring degli eventi non è attivo. |
| MarathonCallRecordExecuted            | AIM_MARATHON_MANAGER_CALLRECORDCOMMAND_ EXECUTED | SNMP_INFO    | Eseguito un comando di controllo registrazione. |
| MarathonCallRecordFailed              | AIM_MARATHON_MANAGER_CALLRECORDCOMMAND_ FAILED | SNMP_ERROR   | Comando di controllo registrazione fallito. |
| MarathonCallInfoUpdated               | AIM_MARATHON_MANAGER_CALLINFO_UPDATED    | SNMP_INFO    | CallInfo sulla chiamata aggiornate.      |
| MarathonUpdateCallInfoFailed          | AIM_MARATHON_MANAGER_CALLINFO_FAILED     | SNMP_ERROR   | Comando di aggiornamento CallInfo fallito. |
| MarathonUpdatedCallInfoDetail         | AIM_MARATHON_MANAGER_CALLINFO_DETAIL     | SNMP_INFO    | Ricevuto evento con dettaglio CallInfo aggiornati. |
| MarathonManagerStopping               | AIM_MARATHON_MANAGER_STOPPING            | SNMP_INFO    | Chiusura del modulo Marathon Manager in corso. |
| MarathonClientReconnect               | AIM_MARATHON_MANAGER_CLIENT_RECONNECT    | SNMP_INFO    | Riconnessione ad API Server in corso.    |
| MarathonCheckConnection               | AIM_MARATHON_MANAGER_CHECK_CONNECTION    | SNMP_INFO    | Verifica connessione con API Server in corso. |
| MarathonConnected                     | AIM_MARATHON_MANAGER_CONNECTED           | SNMP_INFO    | Il modulo MarathonManager è connesso ad API Server. |
| MarathonConnectionThreadStarted       | AIM_MARATHON_MANAGER_CONNECTION_THREAD_ STARTED | SNMP_INFO    | Il thread che gestisce la connessione ad API Server è avviato. |
| MarathonConnectionThreadEnded         | AIM_MARATHON_MANAGER_CONNECTION_THREAD_ ENDED | SNMP_INFO    | Il thread che gestisce la connessione ad API Server è terminato. |
| MarathonUnknownTarget                 | AIM_MARATHON_MANAGER_UNKNOWN_TARGET      | SNMP_WARNING | Il comando ricevuto si riferisce ad un Target sconosciuto. |
| MarathonUnknownTargetType             | AIM_MARATHON_MANAGER_UNKNOWN_TARGET_TYPE | SNMP_WARNING | Il comando ricevuto si riferisce ad una tipologia di Target sconosciuta. |
| MarathonKeepCallError                 | AIM_MARATHON_MANAGER_KEEP_ERROR          | SNMP_ERROR   | Comando Keep fallito.                    |
| MarathonTagCallError                  | AIM_MARATHON_MANAGER_TAG_ERROR           | SNMP_ERROR   | Comando Tag fallito.                     |
| MarathonKeepCallUnknownResult         | AIM_MARATHON_MANAGER_KEEP_UNKNOWN_RESULT | SNMP_ERROR   | Risultato di un comando Keep non riconosciuto. |
| MarathonTagCallUnknownResult          | AIM_MARATHON_MANAGER_TAG_UNKNOWN_RESULT  | SNMP_ERROR   | Risultato di un comando Tag non riconosciuto. |
| MarathonCommandTimeout                | AIM_MARATHON_MANAGER_COMMAND_TIMEOUT     | SNMP_ERROR   | Timeout nell'esecuzione del comando.     |
| MarathonDeleteCallUnknownResult       | AIM_MARATHON_MANAGER_DELETE_UNKNOWN_ RESULT | SNMP_ERROR   | Risultato di un comando Delete non riconosciuto. |
| MarathonDeleteCallError               | AIM_MARATHON_MANAGER_DELETE_ERROR        | SNMP_ERROR   | Comando Delete fallito.                  |
| MarathonStartCallUnknownResult        | AIM_MARATHON_MANAGER_START_UNKNOWN_ RESULT | SNMP_ERROR   | Risultato di un comando Start non riconosciuto. |
| MarathonStartCallError                | AIM_MARATHON_MANAGER_START_ERROR         | SNMP_ERROR   | Comando Start fallito.                   |
| MarathonStopCallUnknownResult         | AIM_MARATHON_MANAGER_STOP_UNKNOWN_RESULT | SNMP_ERROR   | Risultato di un comando Stop non riconosciuto. |
| MarathonStopCallError                 | AIM_MARATHON_MANAGER_STOP_ERROR          | SNMP_ERROR   | Comando Stop fallito.                    |
| MarathonFieldOverwrittenByExtCallID   | AIM_MARATHON_MANAGER_FIELD_OVERWRITTEN_BY_ EXTCALLID | SNMP_WARNING | Un campo è sovrascritto da ExternalCallID. |
| MarathonMuteCallUnknownResult         | AIM_MARATHON_MANAGER_MUTE_UNKNOWN_RESULT | SNMP_ERROR   | Risultato di un comando Mute non riconosciuto. |
| MarathonMuteCallError                 | AIM_MARATHON_MANAGER_MUTE_ERROR          | SNMP_ERROR   | Comando Mute fallito.                    |
| MarathonUnmuteCallUnknownResult       | AIM_MARATHON_MANAGER_UNMUTE_UNKNOWN_ RESULT | SNMP_ERROR   | Risultato di un comando UnMute non riconosciuto. |
| MarathonUnmuteCallError               | AIM_MARATHON_MANAGER_UNMUTE_ERROR        | SNMP_ERROR   | Comando UnMute fallito.                  |
| MarathonInvalidChannelState           | AIM_MARATHON_MANAGER_INVALID_CHANNEL_STATE | SNMP_WARNING | Stato della chiamata non valido per il comando richiesto. |
| MarathonAutoRestartThreadStarted      | AIM_MARATHON_MANAGER_AUTORESTART_THREAD_ STARTED | SNMP_INFO    | Il thread che gestisce la funzione AutoRestart è avviato. |
| MarathonAutoRestartThreadEnded        | AIM_MARATHON_MANAGER_AUTORESTART_THREAD_ ENDED | SNMP_INFO    | Il thread che gestisce la funzione AutoRestart è terminato. |
| MarathonAutorestartChannels           | AIM_MARATHON_MANAGER_AUTORESTART_CHANNELS | SNMP_INFO    | Avvio della funzione AutoRestart.        |
| MarathonAutoRestartingChannel         | AIM_MARATHON_MANAGER_AUTORESTART_CHANNEL | SNMP_INFO    | Avvio della funzione AutoRestart su un canale. |
| MarathonChannelRestarted              | AIM_MARATHON_MANAGER_CHANNEL_RESTARTED   | SNMP_INFO    | Canale riavviato dalla funzione AutoRestart. |
| MarathonFieldOverwrittenByRestartInfo | AIM_MARATHON_MANAGER_FIELD_OVERWRITTEN_ AUTORESTART_INFO | SNMP_WARNING | Un campo è sovrascritto dalle informazioni di AutoRestart. |
| MarathonInvalidAutoRestartInfo        | AIM_MARATHON_MANAGER_INVALID_AUTORESTART_ INFO | SNMP_WARNING | Errore relativo alle informazioni di AutoRestart. |
| MarathonMultipleTargetsFound          | AIM_MARATHON_MANAGER_MULTIPLE_TARGETS_ FOUND | SNMP_INFO    | La ricerca per target restituisce valori multipli. |
| MarathonSingleTargetFound             | AIM_MARATHON_MANAGER_SINGLE_TARGET_FOUND | SNMP_INFO    | La ricerca per target restituisce valori singoli. |
| MarathonTargetFound                   | AIM_MARATHON_MANAGER_SINGLE_TARGET_FOUND | SNMP_INFO    | Target trovato.                          |
| MarathonActiveThreadInfo              | AIM_MARATHON_MANAGER_ACTIVE_THREAD_INFO  | SNMP_INFO    | Informazioni su un thread attivo.        |
| MarathonTestDispatcher                | AIM_MARATHON_MANAGER_DISPATCHER_TEST     | SNMP_INFO    | Test di funzionamento del thread Event Dispatcher. |
| MarathonTestDispatcherUnknownResult   | AIM_MARATHON_MANAGER_DISPATCHER_TEST_ UNKNOWN_RESULT | SNMP_ERROR   | Errore relativo al test di funzionamento del thread Event Dispatcher. |
| MarathonTestDispatcherSuccess         | AIM_MARATHON_MANAGER_DISPATCHER_TEST_OK  | SNMP_INFO    | test di funzionamento del thread Event Dispatcher con esito positivo. |
| MarathonTestDispatcherFailed          | AIM_MARATHON_MANAGER_DISPATCHER_TEST_KO  | SNMP_ERROR   | Errore relativo al test di funzionamento del thread Event Dispatcher. |
| MarathonEmptyChannelIDEventReceived   | AIM_MARATHON_MANAGER_EMPTY_CHANNELID_ EVENT | SNMP_WARNING | Ricevuto un evento non riconducibile ad un canale. |
| MarathonUnmappedChannelID             | AIM_MARATHON_MANAGER_UNMAPPED_EVENT      | SNMP_INFO    | Ricevuto un evento relativo ad un canale non mappato. |
| MarathonChannelMapChanged             | AIM_MARATHON_MANAGER_CHANNEL_MAP_ CHANGED | SNMP_INFO    | Modifica allo mappa interna di stato dei canali. |
| MarathonChannelMapUpdateError         | AIM_MARATHON_MANAGER_CHANNEL_MAP_ERROR   | SNMP_WARNING | Errore nell'aggiornamento della mappa interna di stato dei canali. |
| MarathonInconsistentRequest           | AIM_MARATHON_MANAGER_INCONSISTENT_REQUEST | SNMP_WARNING | La richiesta ricevuta non è più consistente |
| MarathonGetCmdLock                    | AIM_MARATHON_MANAGER_GET_CMD_LOCK        | SNMP_INFO    | In attesa per un blocco su comando.      |
| MarathonCmdLocked                     | AIM_MARATHON_MANAGER_CMD_LOCKED          | SNMP_INFO    | Attesa per blocco su comando terminata.  |
| ProtocolIOError                       | AIM_PROTOCOL_MANAGER_SOCKET_IO_ERROR     | SNMP_ERROR   | Errore di I/O su socket.                 |
| ProtocolNewRequestReceived            | AIM_PROTOCOL_MANAGER_NEW_REQUEST         | SNMP_INFO    | Ricevuto una nuova richiesta.            |
| ProtocolExecuteCommand                | AIM_PROTOCOL_MANAGER_EXECUTE_COMMAND     | SNMP_INFO    | Comando in esecuzione.                   |
| ProtocolExecuteError                  | AIM_PROTOCOL_MANAGER_EXECUTE_ERROR       | SNMP_ERROR   | Errore nell'esecuzione del comando.      |
| ProtocolCommandExecuted               | AIM_PROTOCOL_MANAGER_REQUEST_EXECUTED    | SNMP_INFO    | Richiesta eseguita.                      |
| ProtocolManagerStarting               | AIM_PROTOCOL_MANAGER_STARTING            | SNMP_INFO    | Il modulo Protocol Manager è in fase di avvio. |
| ProtocolListenThreadStarted           | AIM_PROTOCOL_MANAGER_LISTENER_THREAD_STARTED | SNMP_INFO    | Il thread Listener che gestisce le nuove connessioni dai client è avviato. |
| ProtocolListenThreadEnded             | AIM_PROTOCOL_MANAGER_LISTENER_THREAD_ENDED | SNMP_INFO    | Il thread Listener che gestisce le nuove connessioni dai client è terminato. |
| ProtocolConnectionThreadStarted       | AIM_PROTOCOL_MANAGER_CONNECTION_THREAD_ STARTED | SNMP_INFO    | Il thread che gestisce la connessione con il client è avviato. |
| ProtocolConnectionThreadEnded         | AIM_PROTOCOL_MANAGER_CONNECTION_THREAD_ ENDED | SNMP_INFO    | Il thread che gestisce la connessione con il client è terminato. |
| ProtocolConnectionAccepted            | AIM_PROTOCOL_MANAGER_CONNECTION_ACCEPTED | SNMP_AUDIT   | Nuova connessione da client.             |
| ProtocolReceivedMessage               | AIM_PROTOCOL_MANAGER_MESSAGE_RECEIVED    | SNMP_INFO    | Messaggio ricevuto dal client connesso.  |
| ProtocolParseErrorEncoding            | AIM_PROTOCOL_MANAGER_PARSE_ERROR_ENCODING | SNMP_ERROR   | Errore di encoding del messaggio XML.    |
| ProtocolParseErrorXML                 | AIM_PROTOCOL_MANAGER_PARSE_ERROR_FORMAT  | SNMP_ERROR   | Errore generico di parsing del messaggio XML. |
| ProtocolParseErrorValidity            | AIM_PROTOCOL_MANAGER_PARSE_ERROR_VALIDITY | SNMP_ERROR   | Errore di validazione del messaggio XML. |
| ProtocolParseErrorIO                  | AIM_PROTOCOL_MANAGER_PARSE_ERROR_IO      | SNMP_ERROR   | Errore di I/O durante il parsing del messaggio XML. |
| ProtocolParseError                    | AIM_PROTOCOL_MANAGER_PARSE_ERROR         | SNMP_ERROR   | Errore specifico di parsing del messaggio XML rispetto al protocollo AIM. |
| ProtocolParseErrorAttribute           | AIM_PROTOCOL_MANAGER_PARSE_ERROR_WRONG_ ATTRIBUTE | SNMP_ERROR   | Errore relativo ad un attributo erroneo. |
| ProtocolParseErrorAttributeMissing    | AIM_PROTOCOL_MANAGER_PARSE_ERROR_MISSING_ ATTRIBUTE | SNMP_ERROR   | Errore relativo ad un attributo mancante. |
| ProtocolParseErrorElementMissing      | AIM_PROTOCOL_MANAGER_PARSE_ERROR_MISSING_ ELEMENT | SNMP_ERROR   | Errore relativo ad un elemento mancante. |
| ProtocolParseErrorMsgType             | AIM_PROTOCOL_MANAGER_PARSE_ERROR_WRONG_ MESSAGE_TYPE | SNMP_ERROR   | Errore relativo ad una tipologia di messaggio errata. |
| ProtocolParseDone                     | AIM_PROTOCOL_MANAGER_PARSE_COMPLETE      | SNMP_INFO    | Parse del messaggio terminato.           |
| ProtocolSendAnswer                    | AIM_PROTOCOL_MANAGER_SENDING_ANSWER      | SNMP_INFO    | Invio della risposta al client in corso. |
| ProtocolAnswerSent                    | AIM_PROTOCOL_MANAGER_ANSWER_SENT         | SNMP_INFO    | Risposta inviata al client.              |
| ProtocolEmptyTagging                  | AIM_PROTOCOL_MANAGER_EMPTY_TAGGING       | SNMP_INFO    | Richiesta di tagging priva di dati.      |
| ProtocolParseStringTooLong            | AIM_PROTOCOL_MANAGER_PARSE_ERROR_LONG_ STRING | SNMP_WARNING | Parametro stringa di dimensioni maggiori del previsto per il campo specifico. |
| ProtocolParseErrorWrongVersion        | AIM_PROTOCOL_MANAGER_PARSE_ERROR_WRONG_ MESSAGE_TYPE | SNMP_WARNING | Versione del protocollo AIM errata.      |
| ProtocolClientSideCloseSocket         | AIM_PROTOCOL_MANAGER_CLIENT_CLOSED_SOCKET | SNMP_INFO    | Chiusura del socket da parte del client connesso. |
| ProtocolWorkerThreadStarted           | AIM_PROTOCOL_MANAGER_WORKER_THREAD_STARTED | SNMP_INFO    | Il thread che gestisce la coda dei comandi è avviato. |
| ProtocolWorkerThreadEnded             | AIM_PROTOCOL_MANAGER_WORKER_THREAD_STARTED | SNMP_INFO    | Il thread che gestisce la coda dei comandi è terminato. |
| ProtocolMessageEnqueued               | AIM_PROTOCOL_MANAGER_MESSAGE_ENQUEUED    | SNMP_INFO    | Comando messo in coda.                   |
| ProtocolMessageDequeue                | AIM_PROTOCOL_MESSAGE_DEQUEUED            | SNMP_INFO    | Comando prelevato dalla coda.            |
| ProtocolExecuteEnqueuedTask           | AIM_PROTOCOL_MANAGER_EXECUTE_ENQUEUED_TASK | SNMP_INFO    | Comando in coda ora in esecuzione.       |
| WatchdogGo                            | AIM_WATCHDOG_RUNNING                     | SNMP_INFO    | AIM Watchdog è attivo.                   |
| WatchdogConnected                     | AIM_WATCHDOG_CONNECTED_TO_SERVER         | SNMP_INFO    | AIM Watchdog è connesso ad AIM Server.   |
| WatchdogConnectionFailed              | AIM_WATCHDOG_CONNECTION_FAILED           | SNMP_ERROR   | Errore di connessione del Watchdog su AIM Server. |
| WatchdogActionExec                    | AIM_WATCHDOG_ACTION_EXEC                 | SNMP_INFO    | Viene eseguita l'azione specifica per il ripristino di AIM Server. |
| WatchdogActionExecStdOut              | AIM_WATCHDOG_ACTION_EXEC_STD_OUT         | SNMP_INFO    | Dettagli sull'escuzione dell'azione specifica. |
| WatchdogActionExecStdErr              | AIM_WATCHDOG_ACTION_EXEC_STD_ERR         | SNMP_INFO    | Dettagli sull'escuzione dell'azione specifica. |
| WatchdogMessageSent                   | AIM_WATCHDOG_MESSAGE_SENT                | SNMP_INFO    | Messaggio Watchdog inviato ad AIM Server. |
| WatchdogMessageReceived               | AIM_WATCHDOG_ANSWER_RECEIVED             | SNMP_INFO    | Ricevuta risposta da AIM Server.         |
| WatchdogErrorDetected                 | AIM_WATCHDOG_ERROR_DETECTED              | SNMP_WARNING | Il Watchdog ha riscontrato un errore nei test verso AIM Server. |
| WatchdogCannotReadConfig              | AIM_WATCHDOG_CANNOT_READ_CONFIG          | SNMP_ERROR   | Errore relativo alla configurazione del Watchdog. |
| WatchdogDisconnectFailed              | AIM_WATCHDOG_DISCONNECT_FAILED           | SNMP_ERROR   | Errore nella disconnessione del Watchdog da AIM Server. |
| WatchdogIOError                       | AIM_WATCHDOG_IO_ERROR                    | SNMP_ERROR   | Errore di I/O su socket.                 |
| WatchdogParseError                    | AIM_WATCHDOG_PARSE_ERROR                 | SNMP_ERROR   | Errore di parse del messaggio ricevuto.  |
| WatchdogParseMessage                  | AIM_WATCHDOG_MESSAGE_PARSED              | SNMP_INFO    | Parsing del messaggio completato correttamente. |
| WatchdogReceivedInvalidMessage        | AIM_WATCHDOG_RECEIVED_INVALID_MESSAGE    | SNMP_ERROR   | Errore di parse del messaggio ricevuto.  |
| WatchdogException                     | AIM_WATCHDOG_EXCEPTION                   | SNMP_ERROR   | Eccezione rilevata dal watchdog.         |
| WatchdogMaxErrorsReached              | AIM_WATCHDOG_MAX_ERRORS                  | SNMP_ERROR   | Raggiunta la soglia massima di errori consecutivi impostata. |





## Appendice C: Definizioni MIB


Come già evidenziato al §[TODO_link], le trap SNMP inviate dai servizi AIM Server e AIM Watchdog seguono le specifiche ASC. Di seguito le specifiche complete (file `ASC-SNMP-MIB-EXT.txt`):

```
ASC-SNMP-MIB-EXT DEFINITIONS ::= BEGIN

IMPORTS
        OBJECT-TYPE, NOTIFICATION-TYPE,
        MODULE-IDENTITY, OBJECT-IDENTITY, enterprises
                FROM SNMPv2-SMI
        OBJECT-GROUP, NOTIFICATION-GROUP
                FROM SNMPv2-CONF
        DisplayString,TEXTUAL-CONVENTION
                FROM SNMPv2-TC;



 ascEvoModule MODULE-IDENTITY
    LAST-UPDATED "200803190000Z"
    ORGANIZATION "ASC telecom AG"
    CONTACT-INFO
                "       ASC telecom AG
                        Customer Service
 
                Postal: Seibelstrasse 2-4
                       63768 Hoesbach
                        Germany
 
                   Tel: +49 6021 5001 0
 
                 email: hotline@asc.de"
 
	DESCRIPTION
	"This file defines the ASC SNMP MIB extensions."
	REVISION	"200803190000Z"
	DESCRIPTION	"Updated with new status object."
 	REVISION	"200504151200Z"
	DESCRIPTION	"Initial version"

    ::= { ascModule 1}


 asc OBJECT IDENTIFIER ::= { enterprises 4063 }
 ascModule OBJECT IDENTIFIER ::= { asc 1}
 ascProducts OBJECT IDENTIFIER ::= {asc 2 }
 ascRegistration OBJECT IDENTIFIER ::= {asc 3 }
 ascGroups OBJECT IDENTIFIER ::= {asc 4 }
 ascEvoGroups OBJECT IDENTIFIER ::= {ascGroups 1 }



ascEvoReg OBJECT-IDENTITY
        STATUS          current
        DESCRIPTION     "Must be present in sysObjID in RFC1213 MIB for the evolution product"
        ::= { ascRegistration 1 }

ascEvolution OBJECT IDENTIFIER ::= { ascProducts 1 }

ascEvoTrap OBJECT IDENTIFIER ::= { ascEvolution 1 }
ascEvoObj  OBJECT IDENTIFIER ::= { ascEvolution 2 }


AscDateTime ::= TEXTUAL-CONVENTION
        STATUS          current
        DESCRIPTION
        "format is YYYY/MM/DD hh:mm:ss,SSS

       where: YYYY - last four digits of the year (any year)
              MM   - month (01 through 12)
              DD   - day of month (01 through 31)
              hh   - hours (00 through 23)
              mm   - minutes (00 through 59)
              ss   - seconds (00 throug 59)
              SSS  - milliseconds (00 throug 999)"
   

        SYNTAX          OCTET STRING( SIZE( 0..23 ) )
    -- format is YYYY/MM/DD hh:mm:ss,SSS

    --   where: YYYY - last four digits of the year (any year)
    --          MM   - month (01 through 12)
    --          DD   - day of month (01 through 31)
    --          hh   - hours (00 through 23)
    --          mm   - minutes (00 through 59)
    --          ss   - seconds (00 throug 59)
    --          SSS  - milliseconds (00 throug 999)
    --

ascEvoSystemID OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "System ID, this is the unique recorder ID"
    ::= { ascEvoObj 1 }

ascEvoSystemName OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  accessible-for-notify
    STATUS      current
 -- Fin ajout CSSI
    DESCRIPTION
            "System name, the hostname of the recorder"
    ::= { ascEvoObj 2 }

ascEvoModuleName OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Module name, the process which caused the message"
    ::= { ascEvoObj 3 }

ascEvoErrType OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Error type LOG_ERROR, LOG_WARNING, LOG_INFO, LOG_AUDIT"
    ::= { ascEvoObj 4 }

ascEvoErrCode OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Error code"
    ::= { ascEvoObj 5 }

ascEvoErrUniqueID OBJECT-TYPE
    SYNTAX      OCTET STRING (SIZE (0..20))
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Unique error counter"
    ::= { ascEvoObj 6 }

ascEvoErrOpenTime OBJECT-TYPE
    SYNTAX      AscDateTime
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Error opened time"
    ::= { ascEvoObj 7 }

ascEvoErrUpdateTime OBJECT-TYPE
    SYNTAX      AscDateTime
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Error updated time is the same as ascEvoOpenTime on first occurence"
    ::= { ascEvoObj 8 }

ascEvoErrCloseTime OBJECT-TYPE
    SYNTAX      AscDateTime
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Error closed time if closed else empty"
    ::= { ascEvoObj 9 }

ascEvoErrText OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Error text as an additional description"
    ::= { ascEvoObj 10 }

ascEvoErrCloseComment OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  accessible-for-notify
    STATUS      current
    DESCRIPTION
            "Error close comment if closed else empty"
    ::= { ascEvoObj 11 }

ascEvoStatus OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "Status of recorder OK, Warning, Error"
    ::= { ascEvoObj 12 }


ascEvoNotif NOTIFICATION-TYPE
	OBJECTS {
		ascEvoSystemID,
		ascEvoSystemName,
		ascEvoModuleName,
		ascEvoErrType,
		ascEvoErrCode,
		ascEvoErrUniqueID,
		ascEvoErrOpenTime,
		ascEvoErrUpdateTime,
		ascEvoErrCloseTime,
		ascEvoErrText,
		ascEvoErrCloseComment
	}
 	STATUS current
	DESCRIPTION
        "RecorderID:    %s
         Hostname
         Module:        %s
         Type:          %s
         Code:          %s
         Number:        %s
         Opened:        %s
         Updated:       %s
         Closed:        %s
         Text:          %s
         Close Comment: %s"
    ::= {ascEvoTrap 1 }

ascEvoStatusNotif NOTIFICATION-TYPE
	OBJECTS {
		ascEvoSystemID,
		ascEvoSystemName,
		ascEvoStatus
	}
 	STATUS current
	DESCRIPTION
        "RecorderID:	%s
         Hostname:	%s
         Status:	%s"
    ::= {ascEvoTrap 2 }

ascEvoObjGroup  OBJECT-GROUP
        OBJECTS {  
		ascEvoSystemID,
		ascEvoSystemName,
		ascEvoModuleName,
		ascEvoErrType,
		ascEvoErrCode,
		ascEvoErrUniqueID,
		ascEvoErrOpenTime,
		ascEvoErrUpdateTime,
		ascEvoErrCloseTime,
		ascEvoErrText,
		ascEvoErrCloseComment,
		ascEvoStatus
                }
        STATUS          current
        DESCRIPTION "Objects of the ascEvoModule MIB"
                ::=  {  ascEvoGroups  1  }

ascEvoNotifGroup  NOTIFICATION-GROUP
        NOTIFICATIONS {
		ascEvoNotif,
		ascEvoStatusNotif
                   }
        STATUS          current
        DESCRIPTION "Traps Objects of the ascEvoModule MIB"
                ::=  {  ascEvoGroups  2  }

END
```





## Appendice C: Elenco dei software di terze parti utilizzati



**ASC S&R API Java v2.70.03**

Libreria ASC per l'integrazione dei sistemi di registrazione ASC MARATHON EVOLUTION/EVOlite ed EVOip Server con sistemi esterni. 

Licenza: commerciale.



**Apache Log4j 2 v2.9.1**

Libreria per il logging su java.

Link: [https://logging.apache.org/log4j/2.x/](https://logging.apache.org/log4j/2.x/)

Licenza: Apache License v2



**Xom v1.2.10**

Libreria per la gestione XML

Link: [http://www.xom.nu/](http://www.xom.nu/)

Licenza: GNU LGPL v3

​	

**Snmp4j v2.5.8**

Libreria per la gestione SNMP

Link: [http://www.snmp4j.org/](http://www.snmp4j.org/)

Licenza: Apache License v2



**Procrun (Apache Commons Daemon) v1.0.15**

Utility per utilizzare un'applicazione java come servizio Windows.

Link: [https://commons.apache.org/proper/commons-daemon/procrun.html](https://commons.apache.org/proper/commons-daemon/procrun.html)

Licenza: Apache License v2





## Appendice D: Storico versioni

**Ver. 2.0.0**

Porting librerie alle ultime versioni, in particolare:

* Apache Log4j 2 v2.9.1
* Snmp4J v2.5.8
* Xom v1.2.10


Utilizzo di Procrun (Apache Common Daemon) anzichè Java Service Wrapper.


Passaggio a Java 8.

Documentazione in Markdown